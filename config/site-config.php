<?php

return [
	'ti_status' => [
		1 => 'Active',
		0 => 'Inactive',
	],
	'i_question_type' => [
		1 => 'Fill in the blanks',
		2 => 'Multiple choice',
	]
];
