// JavaScript Document
function _loader(action, msg) {
    if (!jQuery(".glob_loader").length) return;
    if (action) jQuery(".glob_loader").removeClass("d-none");
    else jQuery(".glob_loader").addClass("d-none");

    if (msg != undefined && msg)
        jQuery(".glob_loader span").html(msg).removeClass("d-none");
    else jQuery(".glob_loader span").html("").addClass("d-none");
}

function copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
    showAlert(1, "Text Copied ...");
}

function uniqueId(length) {
    var result = "";
    var characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++)
        result += characters.charAt(
            Math.floor(Math.random() * characters.length)
        );
    return result;
}

////////////////////////////////////////////////////////////////////////////

// Alerts
function showAlert(status, message) {
    if (message) {
        jQuery(document).Toasts("create", {
            class: status ? "bg-success" : "bg-danger",
            title: status ? "Success" : "Error",
            //subtitle: 'Subtitle',
            body: message,
            icon: status
                ? "fas fa-check-circle fa-lg"
                : "fas fa-times-circle fa-lg",
            autohide: true,
            delay: 3000,
        });
    }
}

////////////////////////////////////////////////////////////////////////////

function ajaxSucc(res) {
    if (res.message) showAlert(res.status, res.message);
}

function ajaxErr(res) {
    if (res.statusText != "abort") {
        showAlert(0, res.statusText);
    }
}

////////////////////////////////////////////////////////////////////////////

// Select2
function applySelect2(ele) {
    ele.select2({
        width: "100%",
        allowClear: true,
        placeholder: "Please Select",
    });
}
function refreshSelect2(ele) {
    ele.select2("destroy");
    applySelect2(ele);
}

////////////////////////////////////////////////////////////////////////////

// DataTable Functions
function refreshDtTable() {
    jQuery(".crud-check").prop("checked", false);
    jQuery("#crud-table").DataTable().draw(false);
}
function mergeCrudSearch(d) {
    var extra = {};
    if (jQuery("#frm-search-crud").length) {
        var temp = jQuery("#frm-search-crud").serializeArray();
        for (var i in temp) extra[temp[i].name] = temp[i].value;
    }
    return Object.assign(d, extra);
}
function fireCrudSearch() {
    refreshDtTable();
}
function clearCrudSearch() {
    jQuery("#frm-search-crud input:text").val("");
    refreshDtTable();
}
function crudMultiCheck(ele) {
    jQuery(".chk-multi-check").prop("checked", jQuery(ele).is(":checked"));
}
function crudMultiCheckIds() {
    var arr = [];
    jQuery(".chk-multi-check:checked").each(function (index, element) {
        arr.push(jQuery(this).val());
    });
    return arr;
}
function crudDelete(url, type = 1) {
    var ids = [];
    if (type == 1) {
        var ids = crudMultiCheckIds();
        if (!ids.length) {
            showAlert(0, "Please select at least one record.");
            return;
        }
    }

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to Delete this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Delete",
    }).then((result) => {
        if (result.value) {
            _loader(1);
            jQuery.ajax({
                type: "POST",
                url: url,
                data: { ids: ids },
                success: function (res) {
                    if (crudTable != undefined) refreshDtTable();
                    ajaxSucc(res);
                    _loader(0);
                },
                error: function (res) {
                    ajaxErr(res);
                    _loader(0);
                },
            });
        }
    });
}
function crudStatusChange(url, type, flag = 1) {
    if (flag == 1) {
        var ids = crudMultiCheckIds();
        if (!ids.length) {
            showAlert(0, "Please select at least one record.");
            return;
        }

        var text = (btnText = "");
        if (type == 1) {
            text = "You want to activate selected records!";
            btnText = "Active";
        } else if (type == 0) {
            text = "You want to deactivate selected records!";
            btnText = "Inactive";
        }
    } else {
        var text = (btnText = "");
        if (type == 0) {
            text = "You want to activate selected records!";
            btnText = "Active";
        } else if (type == 1) {
            text = "You want to deactivate selected records!";
            btnText = "Inactive";
        }
    }
    Swal.fire({
        title: "Are you sure?",
        text: text,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: btnText,
    }).then((result) => {
        if (result.value) {
            _loader(1);
            jQuery.ajax({
                type: "POST",
                url: url,
                data: { ids: ids, type: type },
                success: function (res) {
                    if (crudTable != undefined) refreshDtTable();
                    ajaxSucc(res);
                    _loader(0);
                },
                error: function (res) {
                    ajaxErr(res);
                    _loader(0);
                },
            });
        }
    });
}
function crudUpdateOrder(url) {
    var arr = {};
    jQuery(".list-order-rows").each(function (index, element) {
        arr[jQuery(this).data("dbid")] = jQuery(this).val();
    });
    _loader(1);
    jQuery.ajax({
        type: "POST",
        url: url,
        data: { data: arr },
        success: function (res) {
            if (crudTable != undefined) refreshDtTable();
            ajaxSucc(res);
            _loader(0);
        },
        error: function (res) {
            ajaxErr(res);
            _loader(0);
        },
    });
}

function checkUnique(type) {
    var v = jQuery.trim(jQuery("#" + type).val());
    if (v) {
        _loader(1, "Checking unique...");
        jQuery.ajax({
            type: "POST",
            url: urlCheckUnique,
            data: { slug: v, type: type },
            success: function (res) {
                //ajaxSucc(res);
                jQuery("#" + type).val(res.slug);
                if (type == "v_sku") jQuery("#uniqueInfoSku").html(res.text);
                else jQuery("#uniqueInfo").html(res.text);
                _loader(0);
            },
            error: function (res) {
                ajaxErr(res);
                _loader(0);
            },
        });
    }
}

////////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function (e) {
    // Setting CSRF Token
    jQuery.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": jQuery('meta[name="csrf-token"]').attr("content"),
        },
        data: {},
        statusCode: {
            //200: function(xhr) { ajaxInvalidAccess(xhr) /*if(window.console) console.log(xhr.responseText);*/ },
            //400: function(xhr) { showGritter({ status: 0, msg: 'Bad Request' }); _loader(0); },
            //401: function(xhr) { showGritter({ status: 0, msg: 'Unauthorized Request - You are not logged in.' }); _loader(0); },
            //403: function(xhr) { showGritter({ status: 0, msg: 'Access Forbidden' }); _loader(0); },
            //404: function(xhr) { showGritter({ status: 0, msg: 'Request Not Found' }); _loader(0); },
            //405: function(xhr) { showGritter({ status: 0, msg: 'Method Not Allowed' }); _loader(0); },
            //406: function(xhr) { showGritter({ status: 0, msg: 'Not Acceptable' }); _loader(0); },
            //407: function(xhr) { showGritter({ status: 0, msg: 'Proxy Authentication Required' }); _loader(0); },
            //408: function(xhr) { showGritter({ status: 0, msg: 'Request Timeout' }); _loader(0); },
            //500: function(xhr) { showGritter({ status: 0, msg: 'Internal Server Error' }); _loader(0); },
            //502: function(xhr) { showGritter({ status: 0, msg: 'Bad Gateway' }); _loader(0); },
            //504: function(xhr) { showGritter({ status: 0, msg: 'Gateway Timeout' }); _loader(0); },
        },
    });

    ////////////////////////////////////////////////////////////////////////////

    // DataTable
    jQuery(document).on("submit", "#frm-search-crud", function () {
        fireCrudSearch();
        return false;
    });
    jQuery(document).on("change", "#frm-search-crud select", function () {
        jQuery("#crud-table").DataTable().draw(false);
    });

    ////////////////////////////////////////////////////////////////////////////

    //Bootstrap Duallistbox
    if (jQuery(".duallistbox").length)
        jQuery(".duallistbox").bootstrapDualListbox({
            moveOnSelect: false,
        });

    ////////////////////////////////////////////////////////////////////////////

    if (jQuery("#crud-frm").length) jQuery("#crud-frm").parsley();
    if (jQuery(".select2").length) applySelect2(jQuery(".select2"));
    if (jQuery(".txteditor").length)
        jQuery(".txteditor").summernote({
            height: 150,
        });

    if (msgSucc) showAlert(1, msgSucc);
    if (msgErr) showAlert(0, msgErr);

    _loader(0);
});


function DeleteQueAns(obj,url, type = 1) {

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to Delete this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Delete",
    }).then((result) => {
        if (result.value) {
            _loader(1);
            jQuery.ajax({
                type: "POST",
                url: url,
                data: {},
                success: function (res) {
                    $(obj).parent().parent().remove();
                    ajaxSucc(res);
                    _loader(0);
                },
                error: function (res) {
                    ajaxErr(res);
                    _loader(0);
                },
            });
        }
    });
}



function DeleteOpts(obj,url, type = 1) {

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to Delete this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Delete",
    }).then((result) => {
        if (result.value) {
            _loader(1);
            jQuery.ajax({
                type: "POST",
                url: url,
                data: {},
                success: function (res) {
                    $(obj).closest('.remove-single-class').remove();
                    ajaxSucc(res);
                    _loader(0);
                },
                error: function (res) {
                    ajaxErr(res);
                    _loader(0);
                },
            });
        }
    });
}