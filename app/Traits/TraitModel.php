<?php
namespace App\Traits;

use Request, Validator;


trait TraitModel{

	// Get Data / Set Data
	public function getId(){ return $this->id ?: 0; }
	
	public function getProductId(){ return $this->i_product_id; }
	public function getSlug(){ return $this->v_slug; }
	
	public function getName(){ return $this->v_name; }
	public function getNameWithStatus(){ return $this->getName().''.(!$this->isActive() ? ' ('.$this->printStatus().')' : ''); }
	
	public function getDescription(){ return $this->l_description; }
	
	public function getStatus(){ return $this->ti_status; }
	public function printStatus(){ return $this->isActive() ? 'Active' : 'Inactive'; }
	public function isActive(){ return $this->getStatus() == 1; }
	public function makeActive(){ $this->ti_status = 1; }
	public function makeInactive(){ $this->ti_status = 0; }
	
	public function getOrder(){ return $this->i_order; }
	public function printOrder(){ return $this->i_order; }
	
	public function setAdded(){ if(!$this->getId()) $this->d_added = dbDate(); }
	public function getAdded(){ return $this->d_added; }
	public function showAdded($datetime=0){ return frDate($this->getAdded(), $datetime); }
	
	public function setUpdated(){ $this->d_updated = dbDate(); }
	public function getUpdated(){ return $this->d_updated; }
	public function showUpdated($datetime=0){ return frDate($this->getUpdated(), $datetime); }
	
	public function putFile($key, $file, $filename=''){
		if($file){
			$oldName = $this->$key;
			if($newName = fileUpload(self::folder(), $file, $filename)){
				if($oldName) 
					fileDelete(self::folder(), $oldName);
				$this->$key = $newName;
			}
		}
	}
	public function getFileName($key){ return $this->$key; }
	public function getFilePath($key, $value=''){ return fileUrl(self::folder(), ($value ?: $this->getFileName($key))); }
	public function getFilePreview($key, $width=50){ return fileHugePreview($this->getFilePath($key), $width); }
	
	
	public function listStatusBadge(){
		if($this->isActive()){
			return '<a href="javascript:void(0);" ><span class="dbadge badge '.($this->isActive() ? 'badge-info' : 'badge-danger').'">'.$this->printStatus().'</span></a>';
		}
	}
	
	public function listOrderBox(){
		return '<input type="number" class="form-control form-control-sm list-order-rows" data-dbid="'.$this->getId().'" value="'.$this->getOrder().'" style="max-width:80px;" min="0" >';
	}
	
}