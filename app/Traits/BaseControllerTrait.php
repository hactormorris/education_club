<?php
namespace App\Traits;

use Request;

trait BaseControllerTrait{

	public function getRouteAction($key='', $default=NULL){
		$action = Request::route()->getAction();
		if( $key && isset( $action[$key] ) ) return ( ( !$action[$key] && $default != NULL ) ? $default : $action[$key] );
		return $action;
	}

	public function notification($key){ return _model('Notification')->prepare($key); }

	// CRUD Functions
	public function index(){
		$model = $this->obj();
		return view($model->info('view_base').'.list', [
			'page_title' => 'List of '.$model->info('multi_name'),
			'model' => $model,
			'items' => $model->all(),
			'breadcrumb' => [
				$model->listUrl() => $model->info('single_name').' List',
			],
		]);
	}

	public function add(){
		$model = $this->obj();
		return view($model->info('view_base').'.form', array(
			'page_title' => 'Add '.$model->info('single_name'),
			'model' => $model,
			'item' => $model,
			'breadcrumb' => [
				$model->listUrl() => $model->info('single_name').' List',
				'' => 'Add '.$model->info('single_name'),
			],
		) );
	}

	public function edit( $id ){
		$model = $this->obj();
		return view($model->info('view_base').'./form', array(
			'page_title' => 'Edit '.$model->info('single_name'),
			'model' => $model,
			'item' => $model->find($id),
			'breadcrumb' => [
				$model->listUrl() => $model->info('single_name').' List',
				'' => 'Edit '.$model->info('single_name'),
			],
		));
	}

	public function delete($id){
		$model = $this->obj();
		$this->obj()->find($id)->removeSelf();
		return redirect()->back()->with('success', trans('messages.crud.deleted', [ 'attribute' => $model->info('single_name') ]));;
	}

	public function checkSlug($id){
		$model = $this->obj();
		if($model->where('v_slug', request('value'))->where('id', '!=', $id)->count()){
			return response( _alert(0, trans('messages.crud.exists', [
				'attribute' => 'Slug'
			]), 1));
		}
		return response( _alert(1, trans('messages.crud.available', [
			'attribute' => 'Slug'
		]), 1));
	}



}