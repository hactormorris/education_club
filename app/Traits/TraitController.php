<?php
namespace App\Traits;

use Request;

trait TraitController{
	
	public function rJson($status=0, $message='', $data=[]){
		return response()->json(array_merge([
			'status' => $status,
			'message' => $message,
		], $data));
	}
	
}