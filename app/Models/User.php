<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
  use Notifiable;
  use \App\Traits\TraitModel;

  protected $table = 'users';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id',
    'name',
    'email',
    'email_verified_at',
    'password',
    'remember_token',
    'ti_status',
    'v_image',
    'created_at',
    'updated_at',
  ];

  protected $attributes = [
    'ti_status' => 1,
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function getName()
  {
    return $this->name;
  }

  public static function makeUrl($type, $args = [])
  {
    return route('admin.adminmanagement.' . $type, $args);
  }

  public static function folder()
  {
    return 'admin';
  }

  public function getImage()
  {
    return $this->v_image;
  }

  public static function boot()
	{
		parent::boot();
		static::deleting(function ($item) {
			if ($val = $item->getImage()) fileDelete(self::folder(), $val);
		});
	}

}
