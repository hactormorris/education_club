<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Customer extends Authenticatable
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_customer';
		
		public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'v_first_name',
        'v_last_name',
        'v_email',
				'v_phone',
        'v_password',
				'ti_status',
        'd_added',
				'd_updated',
    ];
		
		protected $attributes = [
			'ti_status' => 1,
		];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'v_password',
    ];

		public static function boot() {
		parent::boot();
		
			static::creating(function($item) {
				$item->setAdded();
				$item->setUpdated();
			});
	
			static::updating(function($item) {
				$item->setAdded();
				$item->setUpdated();
			});
			
			static::deleting(function($item) {
			});
		}
		
		public static function makeUrl($type, $args=[]){ return route('admin.customer.'.$type, $args); }
    
    public function getFirstName(){ return $this->v_first_name; }
    public function getLastName(){ return $this->v_last_name; }
		public function getName(){ return trim($this->getFirstName().' '.$this->getFirstName()); }
		
    public function getEmail(){ return $this->v_email; }
		public function getPhone(){ return $this->v_phone; }
		
}
