<?php

namespace App\Models;

use Eloquent, Request, Route;

class Media extends Eloquent {

	use \App\Traits\TraitModel;

	protected $table = 'tbl_media';

	public $timestamps = false;

	protected $fillable = [
		'id',
		'v_name',
		'v_file',
	];
	
	public static function boot() {
		parent::boot();
		static::deleting(function($item) {
			if($item->getFile())
				fileDelete(self::folder(), $item->getFile());
		});
	}
	
	public static function makeUrl($type, $args=[]){ return route('admin.media.'.$type, $args); }
	
	public static function folder(){ return 'media'; }
	
	public function getName(){ return $this->v_name; }
	public function getFile(){ return $this->v_file; }
	
	public function getFilePath(){
		return fileUrl(self::folder(), $this->getFile());
	}
	
}