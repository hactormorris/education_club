<?php

namespace App\Models;

use Eloquent, Request, Route;

class QuestionAnswer extends Eloquent
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_question_answer';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'v_question',
        'v_answer',
        'v_options',
        'i_lesson',
        'i_question_type',
        'd_added',
        'd_updated',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::updating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::deleting(function ($item) {
        });
    }
}
