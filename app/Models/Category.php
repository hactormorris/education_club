<?php

namespace App\Models;

use Eloquent, Request, Route;

class Category extends Eloquent {

	use \App\Traits\TraitModel;

	protected $table = 'tbl_category';

	public $timestamps = false;

	protected $fillable = [
		'id',
		'v_slug',
		'v_name',
		'l_description',
		'v_image',
		'ti_status',
		'i_order',
		'd_added',
		'd_updated',
	];
	
	public static function boot() {
		parent::boot();
		
		static::creating(function($item) {
			$item->setAdded();
			$item->setUpdated();
    });

    static::updating(function($item) {
    	$item->setAdded();
			$item->setUpdated();
    });
		
		static::deleting(function($item) {
			if($val = $item->getImage()) fileDelete(self::folder(), $val);
		});
	}
	
	public static function folder(){ return 'category'; }
	public static function makeUrl($type, $args=[]){ return route('admin.category.'.$type, $args); }
	
	public function getImage(){ return $this->v_image; }
	
	public static function dropdown($args=[]){
		$return = [];
		$q = self::query();
		$list = $q->orderBy('ti_status', 'DESC')->orderBy('i_order')->orderBy('v_name')->get();
		foreach($list as $single){
			$return[$single->getId()] = $single->getNameWithStatus();
		}
		return $return;
	}
	
}