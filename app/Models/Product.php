<?php

namespace App\Models;

use Eloquent, Request, Route;

class Product extends Eloquent
{

	use \App\Traits\TraitModel;

	protected $table = 'tbl_product';

	public $timestamps = false;

	protected $fillable = [
		'id',
		'v_slug',
		'v_name',
		'l_description',
		'v_sku',
		'f_price',
		'f_sale_price',
		'i_qty',
		'ti_new_arrival',
		'ti_status',
		'i_order',
		'd_added',
		'd_updated',
	];

	protected $attributes = [
		'ti_status' => 1,
	];

	public static function boot()
	{
		parent::boot();

		static::creating(function ($item) {
			$item->setAdded();
			$item->setUpdated();
		});

		static::updating(function ($item) {
			$item->setAdded();
			$item->setUpdated();
		});

		static::deleting(function ($item) {
			\ProductRelation::where('i_product_id', $item->getId())->delete();
		});
	}

	public static function folder()
	{
		return 'product';
	}

	public static function makeUrl($type, $args = [])
	{
		return route('admin.product.' . $type, $args);
	}

	public function getSku()
	{
		return $this->v_sku;
	}
	public function getPrice()
	{
		return $this->f_price;
	}
	public function getSalePrice()
	{
		return $this->f_sale_price;
	}
	public function getQty()
	{
		return $this->i_qty;
	}
	public function isNewArrival()
	{
		return $this->ti_new_arrival;
	}

	public static function dropdown($args = [])
	{
		$return = [];
		$q = self::query();
		$q = $q->orderBy('ti_status', 'DESC');
		if (isset($args['sku_title']) && $args['sku_title']) {
			$q = $q->orderBy('v_sku');
		}
		$q = $q->orderBy('v_name');
		$list = $q->get();
		foreach ($list as $single) {
			if (isset($args['sku_title']) && $args['sku_title']) {
				$return[$single->getId()] = $single->getSku() . ' (' . $single->getNameWithStatus() . ')';
			} else {
				$return[$single->getId()] = $single->getNameWithStatus();
			}
		}

		return $return;
	}

	public function hasCategories()
	{
		return $this->hasMany('App\Models\ProductRelation', 'i_product_id', 'id')->where('ti_type', \ProductRelation::getCategoryType());
	}
	public function getCategories()
	{
		return $this->hasCategories;
	}
	public function getCategoryIds()
	{
		$return = [];
		foreach ($this->getCategories() as $single)
			$return[] = $single->getTypeId();
		return $return;
	}
	public static function setCategories($productId, $dataIds = [])
	{
		return \ProductRelation::saveMe($productId, 'Category', $dataIds);
	}
}
