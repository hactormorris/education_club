<?php

namespace App\Models;

use Eloquent, Request, Route;

class SeoField extends Eloquent
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_seo_field';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'v_title',
        'v_header_title',
        'v_slug',
        'v_meta_title',
        'v_meta_keywords',
        'l_meta_description',
        'l_description',
        'v_image',
        'l_other_meta',
        'ti_status',
        'd_added',
        'd_updated',
    ];

    protected $attributes = [
        'ti_status' => 1,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::updating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::deleting(function ($item) {
            if ($val = $item->getImage()) fileDelete(self::folder(), $val);
        });
    }

    public static function folder()
    {
        return 'seofield';
    }

    public static function makeUrl($type, $args = [])
    {
        return route('admin.seofield.' . $type, $args);
    }

    public function getImage()
    {
        return $this->v_image;
    }

    public static function dropdown($args = [])
    {
        $return = [];
        $list = self::query()->orderBy('ti_status', 'DESC')->get();
        foreach ($list as $single)
            $return[$single->getId()] = $single->getName() . '' . (!$single->isActive() ? ' (' . $single->printStatus() . ')' : '');
        return $return;
    }
}
