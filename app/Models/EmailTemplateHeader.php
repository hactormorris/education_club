<?php

namespace App\Models;

use Eloquent, Request, Route;

class EmailTemplateHeader extends Eloquent
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_email_template_header';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'v_title',
        'l_description',
        'ti_status',
        'd_added',
        'd_updated',
    ];

    protected $attributes = [
        'ti_status' => 1,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::updating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::deleting(function ($item) {
        });
    }

    public static function makeUrl($type, $args = [])
    {
        return route('admin.emailtemplateheader.' . $type, $args);
    }
    
    public static function dropdown($args = [])
    {
        $return = [];
        $q = self::query();
  
        $list = $q->get();
        foreach ($list as $single) {
            $return[$single->getId()] = $single->v_title;
        }

        return $return;
    }
}
