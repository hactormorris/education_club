<?php

namespace App\Models;

use Eloquent, Request, Route;

class Lesson extends Eloquent
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_lessons';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'i_topic_id',
        'v_title',
        'v_brief_introduction_video',
        'i_question_type',
        'l_description',
        'ti_status',
        'd_added',
        'd_updated',
    ];

    protected $attributes = [
        'ti_status' => 1,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::updating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::deleting(function ($item) {
        });
    }

    public static function makeUrl($type, $args = [])
    {
        return route('admin.lesson.' . $type, $args);
    }

    public function topic()
    {
        return $this->hasOne(Topic::class, 'id', 'i_topic_id');
    }

    public function que_ans()
    {
        return $this->hasMany(QuestionAnswer::class, 'i_lesson', 'id');

    }
}
