<?php

namespace App\Models;

use Eloquent, Request, Route;

class ProductRelation extends Eloquent {

	use \App\Traits\TraitModel;

	protected $table = 'tbl_product_relation';

	public $timestamps = false;

	protected $fillable = [
		'i_product_id',
		'i_type', // 1: Category
		'i_type_id',
	];
	
	public static function boot() {
		parent::boot();
		static::creating(function($item) {
    });
    static::updating(function($item) {
    });
		static::deleting(function($item) {
		});
	}
	
	public function getType(){ return $this->ti_type; }
	public function getTypeId(){ return $this->i_type_id; }
	
	public static function getCategoryType(){ return 1; }
	
	public static function saveMe($productId, $type, $idArr){
		if($type == 'Category') $type = self::getCategoryType();
		
		self::where('ti_type', $type)->where('i_product_id', $productId)->delete();
		$insArr = [];
		foreach($idArr as $newId){
			if($newId) 
				$insArr[] = ['i_product_id' => $productId, 'ti_type' => $type, 'i_type_id' => $newId];
		}
		return $insArr ? self::insert($insArr) : 0;
	}
}