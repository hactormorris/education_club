<?php

namespace App\Models;

use Eloquent, Request, Route;

class Menu extends Eloquent
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_menu';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'v_title',
        'v_link',
        'i_parent_menu',
        'i_order',
        'ti_status',
        'd_added',
        'd_updated',
    ];

    protected $attributes = [
        'ti_status' => 1,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::updating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::deleting(function ($item) {
            \ProductRelation::where('i_product_id', $item->getId())->delete();
        });
    }


    public static function makeUrl($type, $args = [])
    {
        return route('admin.menu.' . $type, $args);
    }

    public static function dropdown($args = [])
    {
        $return = [];
        $q = self::query();
        if (isset($args['i_parent_menu']))
            $q = $q->where('id', '!=', $args['i_parent_menu'])->where('ti_status', '1');
        $list = $q->where('ti_status', '1')->get();
        foreach ($list as $single) {
            $return[$single->getId()] = $single->v_title;
        }
        return $return;
    }
}
