<?php

namespace App\Models;

use Eloquent, Request, Route;

class SiteSetting extends Eloquent {

	use \App\Traits\TraitModel;

	protected $table = 'tbl_site_setting';

	public $timestamps = false;

	public static $dataObjs = [];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	
	protected $fillable = [
		'id',
		'v_key',
		'l_value',
	];
	
	public static function folder(){ return 'general'; }
	public static function fileKeys(){
		return ['logo', 'favicon'];
	}
	
	public function getName(){ return $this->v_key; }
	public function getValue($usable=0){
		if($usable && in_array($this->getName(), self::fileKeys())){
			return fileUrl(self::folder(), $this->l_value);
		}
		return $this->l_value;
	}
	
	public static function fetchObj($key, $usable=0){
		if(!isset(self::$dataObjs[$key]))
			self::$dataObjs[$key] = self::where('v_key', $key)->first();
		if(self::$dataObjs[$key])
			return self::$dataObjs[$key]->getValue($usable);
		return '';
	}
	
	public static function saveMe($key, $value){
		$single = self::where('v_key', $key)->first();
		if($single){
			if(in_array($single->getName(), self::fileKeys()))
				fileDelete(self::folder(), $single->getValue());
		}
		if(!$single) $single = new self;
		$single->v_key = $key;
		$single->l_value = $value;
		$single->save();
	}

}