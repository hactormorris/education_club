<?php

namespace App\Models;

use Eloquent, Request, Route;

class EmailTemplate extends Eloquent
{

    use \App\Traits\TraitModel;

    protected $table = 'tbl_email_template';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'i_header_id',
        'i_footer_id',
        'v_title',
        'v_subject',
        'v_from',
        'l_body',
        'i_delete',
        'ti_status',
        'i_order',
        'd_added',
        'd_updated',
    ];

    protected $attributes = [
        'ti_status' => 1,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::updating(function ($item) {
            $item->setAdded();
            $item->setUpdated();
        });

        static::deleting(function ($item) {
            \ProductRelation::where('i_product_id', $item->getId())->delete();
        });
    }


    public static function makeUrl($type, $args = [])
    {
        return route('admin.emailtemplate.' . $type, $args);
    }
}
