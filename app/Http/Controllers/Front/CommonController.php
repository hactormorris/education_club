<?php

namespace App\Http\Controllers\Front;

//use Request, Session, DB, Route, URL;

use App\Http\Controllers\Controller;

class CommonController extends Controller{
	
	use \App\Traits\TraitController;
	
	public function index(){
		return view('front.pages.index');
	}
	
}