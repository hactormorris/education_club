<?php

namespace App\Http\Controllers\Components;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class S3
{
    /**
     * @param File|UploadedFile $file
     * @param string $remotePath
     * @param string $appCode
     *
     * @return string
     */
    
    public static function uploadFile($file, string $remotePath){


        $diskName = env('S3_DISK') ? env('S3_DISK'):'public';
        $extension = $file->getClientOriginalExtension();
        $fileName = str_replace('.', '', uniqid('', true)) . '.' . $extension;
        $path = Storage::disk($diskName)->putFileAs($remotePath, $file, $fileName,'public');
        
        if ($path === false) {
            throw new \Exception('File was not uploaded.');
        }
        return $path;
    }

    public static function getExtention($file){
        $extension = $file->getClientOriginalExtension();
        return $extension;
    } 

    public static function getFilesize($file){
        $size = $file->getSize();
        return $size;
    } 

    public static function uploadFileBase64($file, string $remotePath){

        $data=$file;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $diskName = env('S3_DISK') ? env('S3_DISK'):'public';
        $extension = "png";
        $fileName = str_replace('.', '', uniqid('', true)) . '.' . $extension;
        $fileName = $remotePath.'/'.$fileName;
        $uploadS3 = Storage::disk($diskName)->put($fileName, $data, 'public');     
        if ($uploadS3 === false) {
            throw new \Exception('File was not uploaded.');
        }
        return $fileName;
    }

     public static function imageAvatar($file, string $remotePath){

        $diskName = env('S3_DISK') ? env('S3_DISK'):'public';
        $extension = "png";
        $fileName = str_replace('.', '', uniqid('', true)) . '.' . $extension;
        $fileName = $remotePath.'/'.$fileName;
        $uploadS3 = Storage::disk($diskName)->put($fileName, $file, 'public');     
        if ($uploadS3 === false) {
            throw new \Exception('File was not uploaded.');
        }
        return $fileName;
    }

    public static function removeFile($file, string $remotePath){
        
        $diskName = env('S3_DISK') ? env('S3_DISK'):'public';
        
        if (Storage::disk($diskName)->Delete($file)) {
            return true;
        }
    }
    
}
