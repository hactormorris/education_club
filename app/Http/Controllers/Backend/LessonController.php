<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;
use App\Models\QuestionAnswer;

class LessonController extends Controller
{

    use \App\Traits\TraitController;

    public function crudTitle($single = 0)
    {
        return $single ? 'Lesson' : 'Lessons';
    }
    public function crudView($type)
    {
        return 'admin.lesson.' . $type;
    }

    public function index()
    {
        if (request('action') == 'list')
            return $this->loadList();
        return view($this->crudView('index'), [
            'title' => $this->crudTitle(),
            'urlAdd' => \Lesson::makeUrl('add'),
            'urlStatusChange' => \Lesson::makeUrl('status-change'),
            'urlDelete' => \Lesson::makeUrl('delete'),
            'urlListData' => \Lesson::makeUrl('index', ['action' => 'list']),
        ]);
    }
    public function loadList()
    {
        $q = \Lesson::query();
        if ($srch = dtblSearch()) {
            $q = $q->where(function ($query) use ($srch) {
                foreach (['v_title', 'ti_status'] as $k => $v) {
                    if (!$k) $query->where($v, 'like', '%' . $srch . '%');
                    else $query->orWhere($v, 'like', '%' . $srch . '%');
                }
            });
        }
        $q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

        $q = $q->orderBy(dtblSortBy([
            'id' => 'id',
            'title' => 'v_title',
            'status' => 'ti_status',
        ], 'd_added'), dtblSortDir('desc'));
        $count = $q->count();

        $data = [];
        $list = $q->skip(dtblStart())->limit(dtblLimit())->get();
        foreach ($list as $single) {

            $url = route("admin.lesson.delete-one", $single->getId());
            $changeStatus = route("admin.lesson.statusupdate", $single->getId());
            $status = '<a onclick="crudStatusChange(\'' . $changeStatus . '\', ' . $single->ti_status . ',2)" href="javascript:void(0);" title="Change Status" class="btn btn-info btn-xs"><i class="' . ($single->ti_status ? 'fas fa-toggle-on' : 'fas fa-toggle-off') . '" style="color:white"></i></a>';
            $deleteBtn = '<a href="javascript:void(0);" title="Delete" onclick="crudDelete(\'' . $url . '\',2)" class="btn btn-info btn-xs"><i class="fas fa-trash" style="color:white"></i></a>';
            $data[] = [
                'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
                'title' => $single->v_title,
                'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',
                'actions' =>
                '<div class="btn-group-sm">
						<a href="' . \Lesson::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
                ' . $deleteBtn . ' 
                ' . $status . '
                        </div>'
            ];
        }
        return $this->rJson(1, '', [
            'draw' => request('draw'),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data,
        ]);
    }

    public function add()
    {
        return $this->form(new \Lesson);
    }
    public function edit($id)
    {
        return $this->form(\Lesson::with('que_ans')->find($id));
    }
    public function form($single)
    {
        return view($this->crudView('form'), [
            'title' => $this->crudTitle(),
            'single' => $single,
            'urlList' => \Lesson::makeUrl('index'),
            'urlSave' => \Lesson::makeUrl('save', ['id' => $single->getId()]),
            // 'urlCheckUnique' => \Lesson::makeUrl('check-unique', ['id' => $single->getId()]),
        ]);
    }

    public function save($id)
    {

        $title = trim(strtolower(request('v_title')));
        if ((\Lesson::where('id', '!=', $id)->where('v_title', $title)->count())) {
            return redirect()->back()->with('error', 'Title already exists.');
        }

        $single = $id ? \Lesson::find($id) : new \Lesson;
        $single->v_title = request('v_title');
        $single->i_topic_id = request('i_topic_id');
		$single->l_description = request('l_description');
        $single->ti_status = request('ti_status');
        $single->v_brief_introduction_video = request('v_brief_introduction_video');
        if (!$id)
            $single->i_question_type = request('i_question_type');
        $single->save();
        $lesson_id = $single->id;
        $arr = [];
        $arr2 = [];

        if (!empty(request('moreFields')) && count(request('moreFields')) && $single->i_question_type == 1) {
            foreach (request('moreFields') as $v) {
                $insertArr =  [
                    'v_question' => $v['question'],
                    'v_answer' => $v['answer'],
                    'i_lesson' => $lesson_id,
                    'i_question_type' => $single->i_question_type,
                    'd_added' => dbDate(),
                    'd_updated' => dbDate()
                ];
                if (isset($v['id']) && $v['id'] != 0) {
                    //  dd('ddd');
                    //   QuestionAnswer::where('id',$v['id'])->update($insertArr);
                    if ($v['question'] != '' && $v['answer'] != '') {
                        $data = \QuestionAnswer::find($v['id']);
                        $data->v_question = $v['question'];
                        $data->v_answer = $v['answer'];
                        $data->i_lesson =  $lesson_id;
                        $data->i_question_type = $single->i_question_type;
                        $data->save();
                    } else {
                        return redirect()->back()->with('error', 'Question Answer Field Not Be Null!');
                    }
                } else {
                    if (isset($v['question']) && isset($v['answer'])) {
                        $arr[] = $insertArr;
                    }
                }
            }
            QuestionAnswer::insert($arr);
        }

        if (!empty(request('mcqs')) && count(request('mcqs')) && $single->i_question_type == 2) {
            foreach (request('mcqs') as $val) {
                // $options = json_encode($val['option']);
                //  dd('ddd');
                if (isset($val['id']) && $val['id'] != 0) {
                    //  dd('ddd');
                    //   QuestionAnswer::where('id',$v['id'])->update($insertArr);
                    if ($val['question'] != '' && $val['answer'] != '') {
                        $data = \QuestionAnswer::find($val['id']);
                        $data->v_question = $val['question'];
                        $data->v_answer = $val['answer'];
                        $data->v_options = json_encode($val['option']);
                        $data->i_lesson =  $lesson_id;
                        $data->i_question_type = $single->i_question_type;
                        $data->save();
                    } else {
                        return redirect()->back()->with('error', 'Question Answer Field Not Be Null!');
                    }
                } else {
                    $insertArray =  [
                        'v_question' => $val['question'],
                        'v_answer' => $val['answer'],
                        'v_options' => json_encode($val['option']),
                        'i_lesson' => $lesson_id,
                        'i_question_type' => $single->i_question_type,
                        'd_added' => dbDate(),
                        'd_updated' => dbDate()
                    ];
                    if (isset($val['question']) && isset($val['answer']) && count($val['option'])) {
                        $arr2[] = $insertArray;
                    }
                }
            }

            QuestionAnswer::insert($arr2);
        }

        return redirect(\Lesson::makeUrl('index'))->with('success', getMsg(($id) ? 'updated' : 'added', ['name' => $this->crudTitle(1)]));
    }

    public function delete()
    {
        $list = \Lesson::find(request('ids', []));
        foreach ($list as $single)
            $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
    }
    public function deleteOne($id)
    {
        $list = \Lesson::find($id)->delete();
        $deleteQueAns=\QuestionAnswer::where('i_lesson',$id)->get();
        foreach($deleteQueAns as $val){
            $val->delete();
        }
        // foreach ($list as $single)
        //     $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle(1)]));
    }
    public function statusChange()
    {
        $type = request('type');
        \Lesson::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
        return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
    }

    public function statusupdate($id)
    {
        \Lesson::where('id', $id)->update(['ti_status' => (request('type') == '1') ? 0 : 1]);
        return $this->rJson(1, getMsg(request('type') ? 'deactivated' : 'activated', ['name' => $this->crudTitle(1)]));
    }

    public function delete_que_ans($id)
    {
        \QuestionAnswer::find($id)->delete();
        return redirect()->back();
    }

    public function delete_option($id, $opts)
    {
        $makeUser = \QuestionAnswer::find($id);
        $numbers_db = $makeUser->v_options;

        $numbers = json_decode($numbers_db, true); //json decode numbers ar
        if (($key = array_search($opts, $numbers)) !== false) {
            unset($numbers[$key]);
            $numbers = array_values($numbers);
        }
        $numbers_final = json_encode($numbers);
        $makeUser->v_options = $numbers_final;
        $makeUser->update();
        return redirect()->back();
        // dd($numbers_final);
        //    \QuestionAnswer::create('v_options',$numbers_final)->where('id',$id);

    }
}
