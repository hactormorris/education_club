<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class EmailTemplateHeaderController extends Controller
{

    use \App\Traits\TraitController;

    public function crudTitle($single = 0)
    {
        return $single ? 'Email Template Header' : 'Email Template Headers';
    }
    public function crudView($type)
    {
        return 'admin.emailtemplateheader.' . $type;
    }

    public function index()
    {
        if (request('action') == 'list')
            return $this->loadList();
        return view($this->crudView('index'), [
            'title' => $this->crudTitle(),
            'urlAdd' => \EmailTemplateHeader::makeUrl('add'),
            'urlStatusChange' => \EmailTemplateHeader::makeUrl('status-change'),
            'urlDelete' => \EmailTemplateHeader::makeUrl('delete'),
            'urlListData' => \EmailTemplateHeader::makeUrl('index', ['action' => 'list']),
        ]);
    }
    public function loadList()
    {
        $q = \EmailTemplateHeader::query();
        if ($srch = dtblSearch()) {
            $q = $q->where(function ($query) use ($srch) {
                foreach (['v_title'] as $k => $v) {
                    if (!$k) $query->where($v, 'like', '%' . $srch . '%');
                    else $query->orWhere($v, 'like', '%' . $srch . '%');
                }
            });
        }
        $q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

        $q = $q->orderBy(dtblSortBy([
            'id' => 'id',
            'title' => 'v_title',
            'status' => 'ti_status',
        ], 'd_added'), dtblSortDir('desc'));
        $count = $q->count();

        $data = [];
        $list = $q->skip(dtblStart())->limit(dtblLimit())->get();
        foreach ($list as $single) {

            $url = route("admin.emailtemplateheader.delete-one", $single->getId());
            $changeStatus = route("admin.emailtemplateheader.statusupdate", $single->getId());
            $status = '<a onclick="crudStatusChange(\'' . $changeStatus . '\', ' . $single->ti_status . ',2)" href="javascript:void(0);" title="Change Status" class="btn btn-info btn-xs"><i class="' . ($single->ti_status ? 'fas fa-toggle-on' : 'fas fa-toggle-off') . '" style="color:white"></i></a>';
            $deleteBtn = '<a href="javascript:void(0);" title="Delete" onclick="crudDelete(\'' . $url . '\',2)" class="btn btn-info btn-xs"><i class="fas fa-trash" style="color:white"></i></a>';
            $data[] = [
                'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
                'title' => $single->v_title,
                'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',
                'actions' =>
                '<div class="btn-group-sm">
						<a href="' . \EmailTemplateHeader::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
                ' . $deleteBtn . ' 
                ' . $status . '
                        </div>'
            ];
        }
        return $this->rJson(1, '', [
            'draw' => request('draw'),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data,
        ]);
    }

    public function add()
    {
        return $this->form(new \EmailTemplateHeader);
    }
    public function edit($id)
    {
        return $this->form(\EmailTemplateHeader::find($id));
    }
    public function form($single)
    {
        return view($this->crudView('form'), [
            'title' => $this->crudTitle(),
            'single' => $single,
            'urlList' => \EmailTemplateHeader::makeUrl('index'),
            'urlSave' => \EmailTemplateHeader::makeUrl('save', ['id' => $single->getId()]),
            // 'urlCheckUnique' => \EmailTemplateHeader::makeUrl('check-unique', ['id' => $single->getId()]),
        ]);
    }

    public function save($id)
    {
        $title = trim(strtolower(request('v_title')));
        if ((\EmailTemplateHeader::where('id', '!=', $id)->where('v_title', $title)->count())) {
            return redirect()->back()->with('error', 'Title already exists.');
        }

        $single = $id ? \EmailTemplateHeader::find($id) : new \EmailTemplateHeader;
        $single->v_title = request('v_title');
        $single->l_description = request('l_description');
        $single->ti_status = request('ti_status');
        $single->save();

        if ($id)
            return redirect(\EmailTemplateHeader::makeUrl('index'))->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
        else
            return redirect(\EmailTemplateHeader::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
    }

    public function delete()
    {
        $list = \EmailTemplateHeader::find(request('ids', []));
        foreach ($list as $single)
            $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
    }
    public function deleteOne($id)
    {
        $list = \EmailTemplateHeader::find($id)->delete();
        // foreach ($list as $single)
        //     $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
    }
    public function statusChange()
    {
        $type = request('type');
        \EmailTemplateHeader::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
        return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle(1)]));
    }

    public function statusupdate($id)
    {
        \EmailTemplateHeader::where('id', $id)->update(['ti_status' => (request('type') == '1') ? 0 : 1]);
        return $this->rJson(1, getMsg(request('type') ? 'deactivated' : 'activated', ['name' => $this->crudTitle(1)]));
    }
}
