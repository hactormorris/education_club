<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    use \App\Traits\TraitController;

    public function crudTitle($single = 0)
    {
        return $single ? 'Admin' : 'Admin';
    }
    public function crudView($type)
    {
        return 'admin.adminmanagement.' . $type;
    }

    public function index()
    {
        if (request('action') == 'list')
            return $this->loadList();
        return view($this->crudView('index'), [
            'title' => $this->crudTitle(),
            'urlAdd' => \User::makeUrl('add'),
            'urlStatusChange' => \User::makeUrl('status-change'),
            'urlDelete' => \User::makeUrl('delete'),
            'urlListData' => \User::makeUrl('index', ['action' => 'list']),
        ]);
    }
    public function loadList()
    {
        $q = \User::query();
        if ($srch = dtblSearch()) {
            $q = $q->where(function ($query) use ($srch) {
                foreach (['name'] as $k => $v) {
                    if (!$k) $query->where($v, 'like', '%' . $srch . '%');
                    else $query->orWhere($v, 'like', '%' . $srch . '%');
                }
            });
        }
        $q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

        $q = $q->orderBy(dtblSortBy([
            'id' => 'id',
            'name' => 'name',
            'email' => 'email',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'status' => 'ti_status',
        ], 'created_at'), dtblSortDir('desc'));
        $count = $q->count();

        $data = [];
        $list = $q->skip(dtblStart())->limit(dtblLimit())->get();
        foreach ($list as $single) {

            $url = route("admin.adminmanagement.delete-one", $single->getId());
            $changeStatus = route("admin.adminmanagement.statusupdate", $single->getId());
            $status = '<a onclick="crudStatusChange(\'' . $changeStatus . '\', ' . $single->ti_status . ',2)" href="javascript:void(0);" title="Change Status" class="btn btn-info btn-xs"><i class="' . ($single->ti_status ? 'fas fa-toggle-on' : 'fas fa-toggle-off') . '" style="color:white"></i></a>';
            $deleteBtn = '<a href="javascript:void(0);" title="Delete" onclick="crudDelete(\'' . $url . '\',2)" class="btn btn-info btn-xs"><i class="fas fa-trash" style="color:white"></i></a>';
            $data[] = [
                'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
                'name' => $single->name,
                'image' =>  '<img src="' . asset("public/uploads/admin/" . $single->v_image) . '" alt="category-Image" height="75" width="75">',
                'email' => $single->email,
                'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',
                'created_at' => date('M d Y', strtotime($single->created_at)),
                'updated_at' => date('M d Y', strtotime($single->updated_at)),
                'actions' =>
                '<div class="btn-group-sm">
						<a href="' . \User::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
                ' . $deleteBtn . ' 
                ' . $status . '
                        </div>'
            ];
        }
        return $this->rJson(1, '', [
            'draw' => request('draw'),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data,
        ]);
    }

    public function add()
    {
        return $this->form(new \User);
    }
    public function edit($id)
    {
        return $this->form(\User::find($id));
    }
    public function form($single)
    {
        return view($this->crudView('form'), [
            'title' => $this->crudTitle(),
            'single' => $single,
            'urlList' => \User::makeUrl('index'),
            'urlSave' => \User::makeUrl('save', ['id' => $single->getId()]),
            // 'urlCheckUnique' => \User::makeUrl('check-unique', ['id' => $single->getId()]),
        ]);
    }

    public function save($id)
    {
        $pass1 = request('v_password');
        $pass2 = request('c_password');

        $title = trim(strtolower(request('v_email')));
        if ((\User::where('id', '!=', $id)->where('email', $title)->count())) {
            return redirect()->back()->with('error', 'Email already exists.');
        }


        if ($pass1 == $pass2) {
            $single = $id ? \User::find($id) : new \User;
            $single->name = request('v_fname');
            $single->email = request('v_email');
            $single->ti_status = request('ti_status');
            $single->password = bcrypt(request('v_password'));
            $single->putFile('v_image', request('v_image'));
            $single->save();

            if ($id)
                return redirect(\User::makeUrl('index'))->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
            else
                return redirect(\User::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
        } else {
            return redirect()->back()->with('error', 'Password & Confirm Password Must Be Same!');
        }
    }


    public function delete()
    {
        $list = \User::find(request('ids', []));
        foreach ($list as $single)
            $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
    }
    public function deleteOne($id)
    {
        $list = \User::find($id)->delete();
        // foreach ($list as $single)
        //     $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
    }
    public function statusChange()
    {
        $type = request('type');
        \User::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
        return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle(1)]));
    }

    public function statusupdate($id)
    {
        \User::where('id', $id)->update(['ti_status' => (request('type') == '1') ? 0 : 1]);
        return $this->rJson(1, getMsg(request('type') ? 'deactivated' : 'activated', ['name' => $this->crudTitle(1)]));
    }
}
