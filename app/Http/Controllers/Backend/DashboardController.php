<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;

use App\Http\Controllers\Controller;

class DashboardController extends Controller{
	
	use \App\Traits\TraitController;
	
	//public function obj(){ return new \ClinicalDeficit; }
	
	public function index(){
		return view('admin.dashboard');
	}
	
		
}