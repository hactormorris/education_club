<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class SubjectController extends Controller
{

    use \App\Traits\TraitController;

    public function crudTitle($single = 0)
    {
        return $single ? 'Subject' : 'Subjects';
    }
    public function crudView($type)
    {
        return 'admin.subject.' . $type;
    }

    public function index()
    {
        if (request('action') == 'list')
            return $this->loadList();
        return view($this->crudView('index'), [
            'title' => $this->crudTitle(),
            'urlAdd' => \Subject::makeUrl('add'),
            'urlStatusChange' => \Subject::makeUrl('status-change'),
            'urlDelete' => \Subject::makeUrl('delete'),
            'urlListData' => \Subject::makeUrl('index', ['action' => 'list']),
        ]);
    }
    public function loadList()
    {
        $q = \Subject::query();
        if ($srch = dtblSearch()) {
            $q = $q->where(function ($query) use ($srch) {
                foreach (['v_title','ti_status'] as $k => $v) {
                    if (!$k) $query->where($v, 'like', '%' . $srch . '%');
                    else $query->orWhere($v, 'like', '%' . $srch . '%');
                }
            });
        }
        $q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

        $q = $q->orderBy(dtblSortBy([
            'id' => 'id',
            'title' => 'v_title',
            'status' => 'ti_status',
        ], 'd_added'), dtblSortDir('desc'));
        $count = $q->count();

        $data = [];
        $list = $q->skip(dtblStart())->limit(dtblLimit())->get();
        foreach ($list as $single) {

            $url = route("admin.subject.delete-one", $single->getId());
            $changeStatus = route("admin.subject.statusupdate", $single->getId());
            $status = '<a onclick="crudStatusChange(\'' . $changeStatus . '\', ' . $single->ti_status . ',2)" href="javascript:void(0);" title="Change Status" class="btn btn-info btn-xs"><i class="' . ($single->ti_status ? 'fas fa-toggle-on' : 'fas fa-toggle-off') . '" style="color:white"></i></a>';
            $deleteBtn = '<a href="javascript:void(0);" title="Delete" onclick="crudDelete(\'' . $url . '\',2)" class="btn btn-info btn-xs"><i class="fas fa-trash" style="color:white"></i></a>';
            $data[] = [
                'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
                'title' => $single->v_title,
                'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',
                'actions' =>
                '<div class="btn-group-sm">
						<a href="' . \Subject::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
                ' . $deleteBtn . ' 
                ' . $status . '
                        </div>'
            ];
        }
        return $this->rJson(1, '', [
            'draw' => request('draw'),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data,
        ]);
    }

    public function add()
    {
        return $this->form(new \Subject);
    }
    public function edit($id)
    {
        return $this->form(\Subject::find($id));
    }
    public function form($single)
    {
        return view($this->crudView('form'), [
            'title' => $this->crudTitle(),
            'single' => $single,
            'urlList' => \Subject::makeUrl('index'),
            'urlSave' => \Subject::makeUrl('save', ['id' => $single->getId()]),
            // 'urlCheckUnique' => \Subject::makeUrl('check-unique', ['id' => $single->getId()]),
        ]);
    }

    public function save($id)
    {
        $title = trim(strtolower(request('v_title')));
        if ((\Subject::where('id', '!=', $id)->where('v_title', $title)->count())) {
            return redirect()->back()->with('error', 'Title already exists.');
        }

        $single = $id ? \Subject::find($id) : new \Subject;
        $single->v_title = request('v_title');
        $single->ti_status = request('ti_status');
        $single->save();

        if ($id)
            return redirect(\Subject::makeUrl('index'))->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
        else
            return redirect(\Subject::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
    }

    public function delete()
    {
        $list = \Subject::find(request('ids', []));
        foreach ($list as $single)
            $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
    }
    public function deleteOne($id)
    {
        $list = \Subject::find($id)->delete();
        // foreach ($list as $single)
        //     $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle(1)]));
    }
    public function statusChange()
    {
        $type = request('type');
        \Subject::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
        return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
    }

    public function statusupdate($id)
    {
        \Subject::where('id', $id)->update(['ti_status' => (request('type') == '1') ? 0 : 1]);
        return $this->rJson(1, getMsg(request('type') ? 'deactivated' : 'activated', ['name' => $this->crudTitle(1)]));
    }
}
