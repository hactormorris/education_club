<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class MediaController extends Controller{
	
	use \App\Traits\TraitController;
	
	public function crudTitle($single=0){ return $single ? 'Media' : 'Media'; }
	public function crudView($type){ return 'admin.media.'.$type; }
	
	public function index(){
		if(request('action') == 'list')
			return $this->loadList();
		return view($this->crudView('index'), [
			'title' => $this->crudTitle(),
			'urlAdd' => \Media::makeUrl('add'),
			'urlDelete' => \Media::makeUrl('delete'),
			'urlListData' => \Media::makeUrl('index', ['action' => 'list']),
		]);
	}
	public function loadList() {
		$q = \Media::query();
		if($srch = dtblSearch()){
			$q = $q->where(function($query) use ($srch){
				foreach(['v_name', 'v_file'] as $k => $v){
					if(!$k) $query->where($v, 'like', '%'.$srch.'%');
					else $query->orWhere($v, 'like', '%'.$srch.'%');
				}
			});
		}
		$q = dtblWhNumeric($q, 'ti_status', request('srch_status'));
		
		
		$q = $q->orderBy(dtblSortBy([
			'id' => 'id',
			'name' => 'v_name',
			'file' => 'v_file',
		], 'v_name'), dtblSortDir());
		$count = $q->count();
		
		$data = [];
		$list = $q->skip(dtblStart())->limit(dtblLimit())->get();
		foreach($list as $single) {
			$fileInfo = [];
			if($path = $single->getFilePath()){
				$fileInfo[] = fileHugePreview($path);
				$fileInfo[] = '<b>PATH:</b> '.$path;
				$fileInfo[] = '<b>COPY:</b> <a href="javascript:;" onclick="copyToClipboard(\''.$path.'\')" ><i class="far fa-copy"></i></a>';
				
			}
			$data[] = [
				'id' => '<input type="checkbox" class="chk-multi-check" value="'.$single->getId().'" />',
				'name' => $single->getName(),
				'file' => $fileInfo ? implode('<br />', $fileInfo) : 'Not Found.'
			];
		}
		return $this->rJson(1, '', [
			'draw' => request('draw'),
			'recordsTotal' => $count,
			'recordsFiltered' => $count,
			'data' => $data,
		]);
	}
	
	public function add(){ return $this->form(new \Media); }
	public function edit($id){ return $this->form(\Media::find($id)); }
	public function form($single){
		return view($this->crudView('form'), [
			'title' => $this->crudTitle(),
			'single' => $single,
			'urlList' => \Media::makeUrl('index'),
			'urlSave' => \Media::makeUrl('save', ['id' => $single->getId()]),
		]);
	}
	
	public function save($id){
		if(!request('v_file')){
			return redirect()->back()->with('error', 'Please upload file.');
		}
		
		$file = request('v_file');
		$fileName = fileUpload(\Media::folder(), $file);
		
		$single = $id ? \Media::find($id) : new \Media;
		$single->v_name = $file->getClientOriginalName();
		$single->v_file = $fileName;
		$single->save();
		return redirect(\Media::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
	}
	
	public function delete(){
		$list = \Media::find(request('ids', []));
		foreach($list as $single) 
			$single->delete();
		return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
	}
		
}