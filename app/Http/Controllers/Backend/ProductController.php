<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class ProductController extends Controller
{

	use \App\Traits\TraitController;

	public function crudTitle($single = 0)
	{
		return $single ? 'Product' : 'Product Products';
	}
	public function crudView($type)
	{
		return 'admin.product.' . $type;
	}

	public function index()
	{
		if (request('action') == 'list')
			return $this->loadList();
		return view($this->crudView('index'), [
			'title' => $this->crudTitle(),
			'urlAdd' => \Product::makeUrl('add'),
			'urlUpdateOrder' => \Product::makeUrl('update-order'),
			'urlStatusChange' => \Product::makeUrl('status-change'),
			'urlDelete' => \Product::makeUrl('delete'),
			'urlListData' => \Product::makeUrl('index', ['action' => 'list']),
		]);
	}
	public function loadList()
	{
		$q = \Product::query();
		if ($srch = dtblSearch()) {
			$q = $q->where(function ($query) use ($srch) {
				foreach (['v_name'] as $k => $v) {
					if (!$k) $query->where($v, 'like', '%' . $srch . '%');
					else $query->orWhere($v, 'like', '%' . $srch . '%');
				}
			});
		}
		$q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

		$q = $q->orderBy(dtblSortBy([
			'id' => 'id',
			'name' => 'v_name',
			'sku' => 'v_sku',
			'price' => 'f_price',
			'sale_price' => 'f_sale_price',
			'qty' => 'i_qty',
			'order' => 'i_order',
			'status' => 'ti_status',
		], 'v_name'), dtblSortDir());
		$count = $q->count();

		$data = [];
		$list = $q->skip(dtblStart())->limit(dtblLimit())->get();

		foreach ($list as $single) {
			$data[] = [
				'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
				'name' => $single->getName(),

				'sku' => $single->getSku(),
				'price' => printPrice($single->getPrice()),
				'sale_price' => printPrice($single->getSalePrice()),
				'qty' => $single->getQty(),

				'order' => $single->listOrderBox(),
				'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',
				'actions' =>
				'<div class="btn-group-sm">
						<a href="' . \Product::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
					</div>'
			];
		}
		return $this->rJson(1, '', [
			'draw' => request('draw'),
			'recordsTotal' => $count,
			'recordsFiltered' => $count,
			'data' => $data,
		]);
	}

	public function add()
	{
		return $this->form(new \Product);
	}
	public function edit($id)
	{
		return $this->form(\Product::find($id));
	}
	public function form($single)
	{
		return view($this->crudView('form'), [
			'title' => $this->crudTitle(),
			'single' => $single,
			'urlList' => \Product::makeUrl('index'),
			'urlSave' => \Product::makeUrl('save', ['id' => $single->getId()]),
			'urlCheckUnique' => \Product::makeUrl('check-unique', ['id' => $single->getId()]),
		]);
	}

	public function save($id)
	{

		$slug = trim(strtolower(seoText(request('v_slug'))));
		if (\Product::where('id', '!=', $id)->where('v_slug', $slug)->count())
			return redirect()->back()->with('error', 'Slug already exists.');

		$sku = trim(request('v_sku'));
		if (\Product::where('id', '!=', $id)->where('v_sku', $sku)->count())
			return redirect()->back()->with('error', 'SKU already exists.');

		$single = $id ? \Product::find($id) : new \Product;
		$single->v_name = request('v_name');
		$single->l_description = request('l_description');
		$single->v_sku = request('v_sku');
		$single->v_slug = request('v_slug');
		$single->f_price = request('f_price', 0);
		$single->f_sale_price = request('f_sale_price', 0);
		$single->i_qty = request('i_qty', 0);
		$single->ti_new_arrival = request('ti_new_arrival', 0);
		$single->ti_status = request('ti_status');
		$single->i_order = request('i_order', 0);
		$single->save();

		// Set Relations
		\Product::setCategories($single->getId(), request('category_ids', []));

		if ($id)
			return redirect()->back()->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
		else
			return redirect(\Product::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
	}

	public function delete()
	{
		$list = \Product::find(request('ids', []));
		foreach ($list as $single)
			$single->delete();
		return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
	}

	public function statusChange()
	{
		$type = request('type');
		\Product::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
		return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
	}

	public function updateOrder()
	{
		$data = request('data', []);
		$list = \Product::whereIn('id', array_keys($data))->get();
		foreach ($list as $row) {
			if (isset($data[$row->getId()])) {
				$row->i_order = $data[$row->getId()] ?: 0;
				$row->save();
			}
		}
		return $this->rJson(1, getMsg('updated', ['name' => 'Order']));
	}

	public function checkUnique()
	{
		if (request('type') == 'v_slug') {
			$slug = trim(strtolower(seoText(request('slug'))));
		} else {
			$slug = trim(request('slug'));
		}

		$arr = [
			'v_sku' => 'SKU',
			'v_slug' => 'Slug',
		];
		$title = $arr[request('type')];

		if ($slug) {
			if (\Product::where('id', '!=', request('id'))->where(request('type'), $slug)->count()) {
				return $this->rJson(0, $title . ' already exists.', ['text' => '<span class="bg-danger p-1">' . $title . ' already exists.</span>']);
			}
			return $this->rJson(1, $title . ' is available.', ['slug' => $slug, 'text' => '<span class="bg-success p-1">' . $title . ' is available.</span>']);
		}
		return $this->rJson(0, $title . ' is required.', ['text' => '<span class="bg-danger p-1">' . $title . ' is required.</span>']);
	}

	public function deleteImage()
	{
		$single = \Product::find(request('id'));
		$delete = fileDelete(\Product::folder(), $single->getImageName(request('image')));

		$image_2 = view('admin.product.preview-image', [
			'single' => $single,
			'num' => 2,
		])->render();

		$viewArr = [];
		for ($i = 3; $i <= \Product::numImages(); $i++) {
			$viewArr[] = view('admin.product.preview-image', [
				'single' => $single,
				'num' => $i,
			])->render();
		}
		return $this->rJson(1, getMsg('deleted', ['name' => 'Image']), [
			'image_2' => $image_2,
			'image_other' => implode('', $viewArr),
			'delete' => $delete,
		]);
	}


	public function bulkImageUpload()
	{
		if ($files = request('images', [])) {
			foreach ($files as $file)
				fileUpload(\Product::folder(), $file, $file->getClientOriginalName(), 1);
			return redirect()->back()->with('success', 'Images uploaded successfully.');
		}
		return view($this->crudView('bulk-image-upload'), [
			'title' => 'Bulk Images Upload',
			'urlList' => \Product::makeUrl('index'),
		]);
	}

	public function import()
	{
		return view($this->crudView('import'), [
			'title' => 'Bulk Product Import/Export',
			'urlList' => \Product::makeUrl('index'),
		]);
	}
}
