<?php

namespace App\Http\Controllers\Backend;;

use Request, Lang, DB, File, Storage, Validator;

use App\Http\Controllers\Controller;
use App\Models\SiteSetting as SitesettingsModel;
use App\Http\Controllers\Components\S3;
use App\Helpers\Emailtemplate as EmailtemplateHelper;

class SiteSettingController extends Controller
{
    protected $section;


    public function index()
    {

        $sitesetting = SitesettingsModel::all();
        // $featuredcategories = FeaturedCategory::all();

        foreach ($sitesetting as $key => $value) {
            if ($value['v_key'] == 'UPLOAD_FILE_TYPES') {
                $data[$value['v_key']] = json_decode($value['l_value']);
            } else {
                if ($value['v_key'] == "NOTIFICATION") {
                    Notification::query()->truncate();
                }
                $data[$value['v_key']] = $value['l_value'];
            }
        }
        $_data = [
            'data'    => $data,
            // 'supplierdata' => $supplier,
            // 'venuedata'   => $venue,
            // "venues"  => $venuemaster,
            // "suppliers" => $suppliermaster
        ];


        return view('admin/settings/site', $_data)->with(['section' => $this->section]);
    }

    public function store()
    {

        $post_data = Request::all();
        // DD($post_data);
        unset($post_data['_token']);

        if (isset($post_data['SEND_TEST_EMAIL']) && $post_data['SITE_MESSAGE'] != "") {

            $fromdata = EmailtemplateHelper::mailFromData();
            $data['USER_FULL_NAME'] = "";
            $mailcontent = $post_data['SITE_MESSAGE'];
            $subject = "Mail Test";
            $mailids = array(
                $data['USER_FULL_NAME'] => $post_data['SEND_TEST_EMAIL']
            );
            $sent = EmailtemplateHelper::MailSendGeneralFinal($fromdata, $subject, $mailcontent, $mailids);
            if ($sent) {
                return redirect()->route('admin.sitesetting.index')->with('success', Lang::get('Mail Has Been Sent!', ['section' => $this->section]));
            } else {
                return redirect()->route('admin.sitesetting.index')->with('warning', Lang::get('Something Went Wrong!', ['section' => $this->section]));
            }
        }

        if (Request::file()) {
            $logo = Request::file('SITE_LOGO');
            if ($logo) {
                $old_sitelogo = EmailtemplateHelper::getSiteSetting('SITE_LOGO');
                if (!empty($old_sitelogo)) {
                    S3::removeFile($old_sitelogo, "common");
                }
                $sitelogo = S3::uploadFile($logo, "common");
                $post_data['SITE_LOGO'] = $sitelogo;
            }
        }

        if (Request::file()) {
            $fvlogo = Request::file('SITE_FAVICON_LOGO');
            if ($fvlogo) {
                $old_fvlogo = EmailtemplateHelper::getSiteSetting('SITE_FAVICON_LOGO');
                if (!empty($old_fvlogo)) {
                    S3::removeFile($old_fvlogo, "common");
                }
                $fvlogo = S3::uploadFile($fvlogo, "common");
                $post_data['SITE_FAVICON_LOGO'] = $fvlogo;
            }
        }

        foreach ($post_data as $key => $value) {

            $siteval = DB::table('tbl_site_setting')->where('v_key', '=', $key)->get();
            if (count($siteval)) {

                $siteval = $siteval[0];
                $update = array(
                    'l_value' => $value,
                );
                SitesettingsModel::find($siteval->id)->update($update);
            } else {
                $insert = array(
                    'v_key' => $key,
                    'l_value' => $value,
                );
                SitesettingsModel::create($insert);
            }
        }
        return redirect()->route('admin.sitesetting.index')->with('success', Lang::get('Site Settings Updated', ['section' => $this->section]));
    }
}
