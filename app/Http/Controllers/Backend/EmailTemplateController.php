<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Http\Controllers\Controller;

class EmailTemplateController extends Controller
{

    use \App\Traits\TraitController;

    public function crudTitle($single = 0)
    {
        return $single ? 'Email Template' : 'Email Templates';
    }
    public function crudView($type)
    {
        return 'admin.emailtemplate.' . $type;
    }

    public function index()
    {
        if (request('action') == 'list')
            return $this->loadList();
        return view($this->crudView('index'), [
            'title' => $this->crudTitle(),
            'urlAdd' => \EmailTemplate::makeUrl('add'),
            'urlStatusChange' => \EmailTemplate::makeUrl('status-change'),
            'urlDelete' => \EmailTemplate::makeUrl('delete'),
            'urlListData' => \EmailTemplate::makeUrl('index', ['action' => 'list']),
        ]);
    }
    public function loadList()
    {
        $q = \EmailTemplate::query();
        if ($srch = dtblSearch()) {
            $q = $q->where(function ($query) use ($srch) {
                foreach (['v_title', 'v_subject'] as $k => $v) {
                    if (!$k) $query->where($v, 'like', '%' . $srch . '%');
                    else $query->orWhere($v, 'like', '%' . $srch . '%');
                }
            });
        }
        $q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

        $q = $q->orderBy(dtblSortBy([
            'id' => 'id',
            'title' => 'v_title',
            'subject' => 'v_subject',
            'created_at' => 'd_added',
            'updated_at' => 'd_updated',
            'status' => 'ti_status',
        ], 'd_added'), dtblSortDir('desc'));
        $count = $q->count();

        $data = [];
        $list = $q->skip(dtblStart())->limit(dtblLimit())->get();

        foreach ($list as $single) {

            $url = route('admin.emailtemplate.single-delete', $single->getId());
            $delete = '<a href="javascript:void(0);" title="Delete" onclick="crudDelete(\'' . $url . '\',2)" class="btn btn-info btn-xs"><i class="fas fa-trash"></i></a>';

            $changeStatus = route("admin.emailtemplate.statusupdate", $single->getId());
            $status = '<a onclick="crudStatusChange(\'' . $changeStatus . '\', ' . $single->ti_status . ',2)" href="javascript:void(0);" title="Change Status" class="btn btn-info btn-xs"><i class="' . ($single->ti_status ? 'fas fa-toggle-on' : 'fas fa-toggle-off') . '" style="color:white"></i></a>';

            $data[] = [
                'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
                'title' => $single->v_title,
                'subject' => $single->v_subject,
                'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',

                'created_at' => date('M d Y', strtotime($single->d_added)),
                'updated_at' => date('M d Y', strtotime($single->d_updated)),
                'actions' =>
                '<div class="btn-group-sm">
                        <a href="' . \EmailTemplate::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
                        ' . $delete . '
                        ' . $status . '
					</div>'
            ];
        }
        return $this->rJson(1, '', [
            'draw' => request('draw'),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data,
        ]);
    }

    public function add()
    {
        return $this->form(new \EmailTemplate);
    }
    public function edit($id)
    {
        return $this->form(\EmailTemplate::find($id));
    }
    public function form($single)
    {
        return view($this->crudView('form'), [
            'title' => $this->crudTitle(1),
            'single' => $single,
            'urlList' => \EmailTemplate::makeUrl('index'),
            'urlSave' => \EmailTemplate::makeUrl('save', ['id' => $single->getId()]),
        ]);
    }

    public function save($id)
    {
        $title = trim(strtolower(request('v_title')));
        if ((\EmailTemplate::where('id', '!=', $id)->where('v_title', $title)->count())) {
            return redirect()->back()->with('error', 'Title already exists.');
        }

        $single = $id ? \EmailTemplate::find($id) : new \EmailTemplate;
        $single->v_title = request('v_title');
        $single->v_subject = request('v_subject');
        $single->ti_status = request('ti_status');
        $single->l_body = request('l_description');
        $single->i_header_id = request('EmailTemplateHeader');
        $single->i_footer_id = request('EmailTemplateFooter');
        $single->i_delete = 0;

        $single->save();


        if ($id)
            return redirect(\EmailTemplate::makeUrl('index'))->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
        else
            return redirect(\EmailTemplate::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
    }

    public function delete()
    {
        $list = \EmailTemplate::find(request('ids', []));
        foreach ($list as $single)
            $single->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle(1)]));
    }

    public function statusChange()
    {
        $type = request('type');
        \EmailTemplate::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
        return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
    }

    public function singleDelete($id)
    {
        $list = \EmailTemplate::find($id)->delete();
        return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle(1)]));
    }

    public function statusUpdate($id)
    {
        \EmailTemplate::where('id', $id)->update(['ti_status' => (request('type') == '1') ? 0 : 1]);
        return $this->rJson(1, getMsg(request('type') ? 'deactivated' : 'activated', ['name' => $this->crudTitle(1)]));
    }

    public function concate()
    {
        $mail_id = array(request('email'));

        $data = array(
            '{USER_FULL_NAME}' => 'Harshkumar Bhavsar',
            '{EMAIL}' => 'harshb37@gmail.com',
            '{CONTACT_NUMBER}' => '9696857414',
        );

        $mail_data = EmailtemplateHelper::EmailTemplateData(23, $data);
        // dd($mail_data);
        $mail_subject = $mail_data['subject'];
        // dd($mail_subject);
        $mail_body    = $mail_data['body'];
        // dd($mail_subject);
        $res = EmailtemplateHelper::sendMail($mail_id, $mail_subject, $mail_body);
        if ($res) {
            return response()->json(array('status' => 1));
        } else {
            return response()->json(array('status' => 0));
        }
    }
}
