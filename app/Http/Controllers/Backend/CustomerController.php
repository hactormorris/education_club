<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class CustomerController extends Controller{
	
	use \App\Traits\TraitController;
	
	public function crudTitle($single=0){ return $single ? 'Customer' : 'Customers'; }
	public function crudView($type){ return 'admin.customer.'.$type; }
	
	public function index(){
		if(request('action') == 'list')
			return $this->loadList();
		return view($this->crudView('index'), [
			'title' => $this->crudTitle(),
			'urlAdd' => \Customer::makeUrl('add'),
			'urlStatusChange' => \Customer::makeUrl('status-change'),
			'urlDelete' => \Customer::makeUrl('delete'),
			'urlListData' => \Customer::makeUrl('index', ['action' => 'list']),
		]);
	}
	public function loadList() {
		$q = \Customer::query();
		if($srch = dtblSearch()){
			$q = $q->where(function($query) use ($srch){
				foreach(['v_name', 'v_email', 'v_phone'] as $k => $v){
					if(!$k) $query->where($v, 'like', '%'.$srch.'%');
					else $query->orWhere($v, 'like', '%'.$srch.'%');
				}
			});
		}
		$q = dtblWhNumeric($q, 'ti_status', request('srch_status'));
		
		$orderBy = dtblSortBy([
			'id' => 'id',
			'name' => 'v_first_name',
			'email' => 'v_email',
			'phone' => 'v_phone',
			'status' => 'ti_status',
		], 'v_first_name');
		
		$q = $q->orderBy($orderBy, dtblSortDir());
		if($orderBy == 'v_first_name') 
			$q = $q->orderBy('v_last_name', dtblSortDir());
		
		$count = $q->count();
		
		$data = [];
		$list = $q->skip(dtblStart())->limit(dtblLimit())->get();
		foreach($list as $single) {
			$data[] = [
				'id' => '<input type="checkbox" class="chk-multi-check" value="'.$single->getId().'" />',
				'name' => _val($single->getName()),
				'email' => _val($single->getEmail()),
				'phone' => _val($single->getPhone()),
				'status' => '<span class="dbadge badge '.($single->isActive() ? 'badge-info' : 'badge-danger').'">'.$single->printStatus().'</span>',
				'actions' => 
					'<div class="btn-group-sm">
						<a href="'.\Customer::makeUrl('edit', ['id' => $single->getId()]).'" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
					</div>'
			];
		}
		return $this->rJson(1, '', [
			'draw' => request('draw'),
			'recordsTotal' => $count,
			'recordsFiltered' => $count,
			'data' => $data,
		]);
	}
	
	public function add(){ return $this->form(new \Customer); }
	public function edit($id){ return $this->form(\Customer::find($id)); }
	public function form($single){
		return view($this->crudView('form'), [
			'title' => $this->crudTitle(),
			'single' => $single,
			'urlList' => \Customer::makeUrl('index'),
			'urlSave' => \Customer::makeUrl('save', ['id' => $single->getId()]),
			'urlCheckUnique' => \Customer::makeUrl('check-unique', ['id' => $single->getId()]),
		]);
	}
	
	public function save($id){
		$slug = trim(strtolower(seoText(request('v_code'))));
		if(\Customer::where('id', '!=', $id)->where('v_email', $slug)->count()){
			return redirect()->back()->with('error', 'Email already exists.');
		}
		
		$single = $id ? \Customer::find($id) : new \Customer;
		$single->v_first_name = request('v_first_name');
		$single->v_last_name = request('v_last_name');
		$single->v_email = request('v_email');
		$single->v_phone = request('v_phone');
		if(request('i_update_password'))
			$single->v_password = request('v_password') ? bcrypt(request('v_password')) : NULL;
		$single->ti_status = request('ti_status');
		$single->save();
		
		if($id)
			return redirect()->back()->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
		else 
			return redirect(\Customer::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
	}
	
	public function delete(){
		$list = \Customer::find(request('ids', []));
		foreach($list as $single)
			$single->delete();
		return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
	}
	
	public function statusChange(){
		$type = request('type');
		\Customer::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
		return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
	}
	
	public function checkUnique(){
		$slug = trim(strtolower(seoText(request('slug'))));
		if($slug){
			if(\Customer::where('id', '!=', request('id'))->where('v_email', $slug)->count()){
				return $this->rJson(0, 'Email already exists.', ['text' => '<span class="bg-danger p-1">Email already exists.</span>']);
			}
			return $this->rJson(1, 'Email is available.', ['slug' => $slug, 'text' => '<span class="bg-success p-1">Email is available.</span>']);
		}
		return $this->rJson(0, 'Email is required.', ['text' => '<span class="bg-danger p-1">Email is required.</span>']);
	}
	
		
}