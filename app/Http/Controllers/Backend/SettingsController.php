<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class SettingsController extends Controller{
	
	use \App\Traits\TraitController;
	
	public function site(){
		return view('admin.settings.site', [
			'title' => 'Site Settings'
		]);
	}
	public function analytics(){
		return view('admin.settings.analytics', [
			'title' => 'Analytics Settings'
		]);
	}
	
	public function submit(){
		foreach(request()->all() as $key => $value){
			if(in_array($key, ['_token']))
				continue;
			if(in_array($key, \SiteSetting::fileKeys())){
				$value = fileUpload(\SiteSetting::folder(), $value);
			}
			\SiteSetting::saveMe($key, $value);
		}
		return redirect()->back()->with('success', getMsg('updated_settings'));
	}
	
		
}