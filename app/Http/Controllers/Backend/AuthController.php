<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon, View, SimpleXMLElement, DOMDocument;

use App\Models\Admin;
use App\Models\AdminLog;

class AuthController extends Controller{

	use \App\Traits\TraitController;
	
	public function __construct(){
	}

	public function login(){
		if(_admin('id')) 
			return redirect()->route('admin.dashboard');
		return view('admin/auth/login', []);
	}

	public function loginSubmit(){
		if(_admin('id'))
			return redirect()->route('admin.dashboard');
		$auth = auth()->guard('admin');
		if($auth->attempt(request()->only('email', 'password'), request('remember_me', false))){
			if(_admin()){
				if(!_admin()->isActive()){
					auth()->guard('admin')->logout();
					return redirect()->back()->with('error', 'Your account is deactiated.');
				}
				return redirect(request('backurl', route('admin.dashboard')))->with('success', trans('auth.success'));
			}
			auth()->guard('admin')->logout();
		}
		return redirect()->back()->with('error', trans('auth.failed'));
	}

	public function logout(){
		if(_admin('id')){
			auth()->guard('admin')->logout();
		}
		return redirect()->route('admin.login');//->with('success', trans('messages.logout'));
	}

	
}
