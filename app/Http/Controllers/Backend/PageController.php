<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class PageController extends Controller
{

	use \App\Traits\TraitController;

	public function crudTitle($single = 0)
	{
		return $single ? 'Page Detail' : 'Page Details';
	}
	public function crudView($type)
	{
		return 'admin.page.' . $type;
	}

	public function index()
	{
		if (request('action') == 'list')
			return $this->loadList();
		return view($this->crudView('index'), [
			'title' => $this->crudTitle(),
			'urlAdd' => \Page::makeUrl('add'),
			'urlStatusChange' => \Page::makeUrl('status-change'),
			'urlDelete' => \Page::makeUrl('delete'),
			'urlListData' => \Page::makeUrl('index', ['action' => 'list']),
		]);
	}
	public function loadList()
	{
		$q = \Page::query();
		if ($srch = dtblSearch()) {
			$q = $q->where(function ($query) use ($srch) {
				foreach (['v_title', 'v_slug', 'v_header_title', 'v_meta_keywords'] as $k => $v) {
					if (!$k) $query->where($v, 'like', '%' . $srch . '%');
					else $query->orWhere($v, 'like', '%' . $srch . '%');
				}
			});
		}
		$q = dtblWhNumeric($q, 'ti_status', request('srch_status'));

		$q = $q->orderBy(dtblSortBy([
			'id' => 'id',
			'title' => 'v_title',
			'HeaderTitle' => 'v_header_title',
			'PageSlug' => 'v_slug',
			'MetaTitle' => 'v_meta_title',
			'MetaKeywords' => 'v_meta_keywords',
			'status' => 'ti_status',
		], 'd_added'), dtblSortDir('desc'));
		$count = $q->count();

		$data = [];
		$list = $q->skip(dtblStart())->limit(dtblLimit())->get();
		foreach ($list as $single) {
			$url = route('admin.page.delete-one', $single->getId());
			$delete = '<a href="javascript:void(0);" title="Delete" onclick="crudDelete(\'' . $url . '\',2)" class="btn btn-info btn-xs"><i class="fas fa-trash" style="color:white"></i></a>';

			$url1 = route('admin.page.statusupdate', $single->getId());
			$status = '<a onclick="crudStatusChange(\'' . $url1 . '\', ' . $single->ti_status . ',2)" href="javascript:void(0);" title="Change Status" class="btn btn-info btn-xs"><i class="' . ($single->ti_status ? 'fas fa-toggle-on' : 'fas fa-toggle-off') . '" style="color:white"></i></a>';
			$data[] = [
				'id' => '<input type="checkbox" class="chk-multi-check" value="' . $single->getId() . '" />',
				'image' =>  '<img src="' . asset("public/uploads/page/" . $single->v_image) . '" alt="category-Image" height="75" width="75">',
				'title' => $single->v_title,
				'HeaderTitle' => $single->v_header_title,
				'PageSlug' => $single->v_slug,
				'MetaTitle' => $single->v_meta_title,
				'MetaKeywords' => $single->v_meta_keywords,
				'status' => '<span class="dbadge badge ' . ($single->isActive() ? 'badge-info' : 'badge-danger') . '">' . $single->printStatus() . '</span>',
				'actions' =>
				'<div class="btn-group-sm">
						<a href="' . \Page::makeUrl('edit', ['id' => $single->getId()]) . '" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
				' . $delete . '	
				' . $status . '	
				</div>'
			];
		}
		return $this->rJson(1, '', [
			'draw' => request('draw'),
			'recordsTotal' => $count,
			'recordsFiltered' => $count,
			'data' => $data,
		]);
	}

	public function add()
	{
		return $this->form(new \Page);
	}
	public function edit($id)
	{
		return $this->form(\Page::find($id));
	}
	public function form($single)
	{
		return view($this->crudView('form'), [
			'title' => $this->crudTitle(),
			'single' => $single,
			'urlList' => \Page::makeUrl('index'),
			'urlSave' => \Page::makeUrl('save', ['id' => $single->getId()]),
			'urlCheckUnique' => \Page::makeUrl('check-unique', ['id' => $single->getId()]),
		]);
	}

	public function save($id)
	{
		$slug = trim(strtolower(seoText(request('v_slug'))));
		if (\Page::where('id', '!=', $id)->where('v_slug', $slug)->count()) {
			return redirect()->back()->with('error', 'Slug already exists.');
		}

		$single = $id ? \Page::find($id) : new \Page;
		$single->v_slug = $slug;
		$single->v_title = request('v_title');
		$single->v_slug = request('v_slug');
		$single->v_header_title = request('v_header_title');
		$single->ti_status = request('ti_status');
		$single->v_meta_title = request('v_meta_title');
		$single->v_meta_keywords = request('v_meta_keywords');
		$single->l_meta_description = request('l_meta_description');
		$single->l_other_meta = request('l_other_meta');
		$single->l_description = request('l_description');
		$single->putFile('v_image', request('v_image'));
		$single->save();

		if ($id)
			return redirect(\Page::makeUrl('index'))->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
		else
			return redirect(\Page::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
	}

	public function delete()
	{
		$list = \Page::find(request('ids', []));
		foreach ($list as $single)
			$single->delete();
		return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
	}

	public function statusChange()
	{
		$type = request('type');
		\Page::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
		return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
	}

	public function checkUnique()
	{
		$slug = trim(strtolower(seoText(request('slug'))));
		if ($slug) {
			if (\Page::where('id', '!=', request('id'))->where('v_slug', $slug)->count()) {
				return $this->rJson(0, 'Slug already exists.', ['text' => '<span class="bg-danger p-1">Slug already exists.</span>']);
			}
			return $this->rJson(1, 'Slug is available.', ['slug' => $slug, 'text' => '<span class="bg-success p-1">Slug is available.</span>']);
		}
		return $this->rJson(0, 'Slug is required.', ['text' => '<span class="bg-danger p-1">Slug is required.</span>']);
	}

	public function statusupdate($id)
	{
		\Page::where('id', $id)->update(['ti_status' => (request('type') == '1') ? 0 : 1]);
		return $this->rJson(1, getMsg(request('type') ? 'deactivated' : 'activated', ['name' => $this->crudTitle(1)]));
	}

	public function deleteOne($id)
	{
		$list = \Page::find($id)->delete();
		// foreach ($list as $single)
		//     $single->delete();
		return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle(1)]));
	}
}
