<?php

namespace App\Http\Controllers\Backend;

//use Request, Session, DB, Route, URL;
use Storage;

use App\Http\Controllers\Controller;

class CategoryController extends Controller{
	
	use \App\Traits\TraitController;
	
	public function crudTitle($single=0){ return $single ? 'Category' : 'Categories'; }
	public function crudView($type){ return 'admin.category.'.$type; }
	
	public function index(){
		if(request('action') == 'list')
			return $this->loadList();
		return view($this->crudView('index'), [
			'title' => $this->crudTitle(),
			'urlAdd' => \Category::makeUrl('add'),
			'urlStatusChange' => \Category::makeUrl('status-change'),
			'urlUpdateOrder' => \Category::makeUrl('update-order'),
			'urlDelete' => \Category::makeUrl('delete'),
			'urlListData' => \Category::makeUrl('index', ['action' => 'list']),
		]);
	}
	public function loadList() {
		$q = \Category::query();
		if($srch = dtblSearch()){
			$q = $q->where(function($query) use ($srch){
				foreach(['v_name', 'v_slug'] as $k => $v){
					if(!$k) $query->where($v, 'like', '%'.$srch.'%'); else $query->orWhere($v, 'like', '%'.$srch.'%');
				}
			});
		}
		$q = dtblWhNumeric($q, 'ti_status', request('srch_status'));
		
		$q = $q->orderBy(dtblSortBy([
			'name' => 'v_name',
			'order' => 'i_order',
			'status' => 'ti_status',
		], 'v_name'), dtblSortDir());
		$count = $q->count();
		
		$data = [];
		$list = $q->skip(dtblStart())->limit(dtblLimit())->get();
		
		foreach($list as $single) {
			$data[] = [
				'id' => '<input type="checkbox" class="chk-multi-check" value="'.$single->getId().'" />',
				'name' => _val($single->getName()),
				'order' => $single->listOrderBox(),
				'status' => '<span class="dbadge badge '.($single->isActive() ? 'badge-info' : 'badge-danger').'">'.$single->printStatus().'</span>',
				'actions' => 
					'<div class="btn-group-sm">
						<a href="'.\Category::makeUrl('edit', ['id' => $single->getId()]).'" title="Edit" class="btn btn-info btn-xs"><i class="fas fa-edit"></i></a>
					</div>'
			];
		}
		
		return $this->rJson(1, '', [
			'draw' => request('draw'),
			'recordsTotal' => $count,
			'recordsFiltered' => $count,
			'data' => $data
		]);
	}
	
	
	public function add(){ return $this->form(new \Category); }
	public function edit($id){ return $this->form(\Category::find($id)); }
	public function form($single){
		return view($this->crudView('form'), [
			'title' => $this->crudTitle(),
			'single' => $single,
			'urlList' => \Category::makeUrl('index'),
			'urlSave' => \Category::makeUrl('save', ['id' => $single->getId()]),
			'urlCheckUnique' => \Category::makeUrl('check-unique', ['id' => $single->getId()]),
		]);
	}
	
	public function save($id){
		$slug = trim(strtolower(seoText(request('v_slug'))));
		if(\Category::where('id', '!=', $id)->where('v_slug', $slug)->count()){
			return redirect()->back()->with('error', 'Slug already exists.');
		}
		
		$single = $id ? \Category::find($id) : new \Category;
		$single->v_slug = $slug;
		$single->v_name = request('v_name');
		$single->l_description = request('l_description');
		$single->ti_status = request('ti_status');
		$single->i_order = request('i_order');
		$single->putFile('v_image', request('v_image'));
		$single->save();
		
		if($id)
			return redirect()->back()->with('success', getMsg('updated', ['name' => $this->crudTitle(1)]));
		else 
			return redirect(\Category::makeUrl('index'))->with('success', getMsg('added', ['name' => $this->crudTitle(1)]));
	}
	
	public function delete(){
		$list = \Category::find(request('ids', []));
		foreach($list as $single)
			$single->delete();
		return $this->rJson(1, getMsg('deleted', ['name' => $this->crudTitle()]));
	}
	
	public function statusChange(){
		$type = request('type');
		\Category::whereIn('id', request('ids', []))->update(['ti_status' => $type]);
		return $this->rJson(1, getMsg($type ? 'activated' : 'deactivated', ['name' => $this->crudTitle()]));
	}
	
	public function updateOrder(){
		$data = request('data', []);
		$list = \Category::whereIn('id', array_keys($data))->get();
		foreach($list as $row){
			if(isset($data[$row->getId()])){
				$row->i_order = $data[$row->getId()] ?: 0;
				$row->save();
			}
		}
		return $this->rJson(1, getMsg('updated', ['name' => 'Order']));
	}
	
	public function checkUnique(){
		$slug = trim(strtolower(seoText(request('slug'))));
		if($slug){
			if(\Category::where('id', '!=', request('id'))->where('v_slug', $slug)->count()){
				return $this->rJson(0, 'Slug already exists.', ['text' => '<span class="bg-danger p-1">Slug already exists.</span>']);
			}
			return $this->rJson(1, 'Slug is available.', ['slug' => $slug, 'text' => '<span class="bg-success p-1">Slug is available.</span>']);
		}
		return $this->rJson(0, 'Slug is required.', ['text' => '<span class="bg-danger p-1">Slug is required.</span>']);
	}
	
		
}