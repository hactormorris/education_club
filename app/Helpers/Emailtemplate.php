<?php

namespace App\Helpers;

use Request, Validator, Session, DB, Auth, Hash, Carbon\Carbon, Crypt;
use App\Models\SiteSetting as SiteSettingsModel;
use App\Models\Settings\HeaderImages as HeaderImagesModel;
use App\Models\Plan as PlanModel;
use App\Models\EmailTemplate as EmailTemplateModel;
use App\Models\EmailTemplateHeader as EmailTemplateHeader;
use App\Models\EmailTemplateFooter as EmailTemplateFooter;
use App\Models\EmailTemplate\Emailsend;
use App\Models\Admin\Admin as AdminModel;
use Illuminate\Database\Eloquent\Collection;
use PHPMailerAutoload;
// use PHPMailer;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;


class Emailtemplate
{

    public static function mailFromData()
    {

        $site_data = SiteSettingsModel::get();
        $fromdata = array();
        if ($site_data->count()) {
            foreach ($site_data as $key => $value) {
                if ($value->v_key == "SMTP_HOST") {
                    $fromdata['host'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_PROTOCOL") {
                    $fromdata['protocol'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_PORT") {
                    $fromdata['port'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_USERNAME") {
                    $fromdata['username'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_PASSWORD") {
                    $fromdata['password'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_FROM_EMAIL") {
                    $fromdata['from_email'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_FROM_NAME") {
                    $fromdata['from_name'] = $value->l_value;
                }
                if ($value->v_key == "SMTP_REPLY_EMAIL") {
                    $fromdata['replay_email'] = $value->l_value;
                }
            }
        }
        return $fromdata;
    }



    public static function EmailTemplateData($id = "", $keyData = array())
    {

        $response = array();
        $mail_data = EmailTemplateModel::find($id);
        if ($mail_data->count() == 0) {
            return 0;
        }
        $response['subject'] = $mail_data->v_subject;
        $response['body'] = "";

        if (isset($mail_data->i_header_id) && $mail_data->i_header_id != "") {
            $templateHeader = EmailTemplateHeader::find($mail_data->i_header_id);
            if ($templateHeader->count()) {
                $response['body'] .= $templateHeader->l_description;
            } else {
                $response['body'] .= self::EmailTemplateHeaderDefine();
            }
        } else {
            $response['body'] .= self::EmailTemplateHeaderDefine();
        }

        if (isset($mail_data->l_body) && $mail_data->l_body != "") {
            $response['body'] .= $mail_data->l_body;
        }

        if (isset($mail_data->i_footer_id) && $mail_data->i_footer_id != "") {

            $templateFooter = EmailTemplateFooter::find($mail_data->i_footer_id);

            if ($templateFooter->count()) {
                $response['body'] .= $templateFooter->l_description;
            } else {
                $response['body'] .= self::EmailTemplateFooterDefine();
            }
        } else {
            $response['body'] .= self::EmailTemplateFooterDefine();
        }

        if (count($keyData)) {
            foreach ($keyData as $key => $value) {
                $response['body'] = str_replace($key, $value, $response['body']);
            }
        }
        return $response;
    }


    public static function EmailTemplateHeaderDefine()
    {

        $headerstr = "";
        $headerstr .= '<table style="width: 100%; max-width: 500px; margin: 0px auto;">';
        $headerstr .= '<tbody>';
        $headerstr .= '<tr>';
        $headerstr .= '<td>';
        $headerstr .= '<table>';
        $headerstr .= '<tbody>';
        $headerstr .= '<tr><td>';
        $headerstr .= '<img src="https://designguidelines.withgoogle.com/ads-branding/assets/1IWXAGxcyU5sBaUKU3yGXPztyqCm-WM8H/ads-logo-horizontal-dont-2.png" height="200px" width="350px"/></td>';
        $headerstr .= '</tr>';
        $headerstr .= '</tbody>';
        $headerstr .= '</table>';
        $headerstr .= '<hr style="border: 2px solid black;" /></td>';
        $headerstr .= '</tr>';
        $headerstr .= '</tbody>';
        $headerstr .= '</table>';
        return $headerstr;
    }


    public static function EmailTemplateFooterDefine()
    {

        $footerstr = "";
        $footerstr .= '<div style="width: 100%; max-width: 500px; margin: 0px auto;">';
        $footerstr .= '<hr style="border: 2px solid black;" />';
        $footerstr .= '<table style="margin-top: 30px; width: 100%;">';
        $footerstr .= '<tbody>';
        $footerstr .= '<tr>';
        $footerstr .= '<td align="center"><a href="{_unsubscribe_link}">Unsubscribe</a></td>';
        $footerstr .= '</tr>';
        $footerstr .= '</tbody>';
        $footerstr .= '</table>';
        $footerstr .= '<hr style="border: 2px solid black;" />';
        $footerstr .= '<table style="margin-top: 30px; width: 100%;">';
        $footerstr .= '<tbody>';
        $footerstr .= '<tr><td>';
        $footerstr .= '<img src="https://www.freepnglogos.com/uploads/shopee-logo-png/shopee-logo-shopee-and-lazada-philippines-fake-counterfeit-products-4.png" height="200px" width="350px" /></td>';
        $footerstr .= '<td style="font-size: 9px; text-align: right; line-height: 12px; width: 25%;"><span>Education Club</span><br />';
        $footerstr .= '<span>PO BOX 7153 </span><br />';
        $footerstr .= '<span>Lichfield </span><br />';
        $footerstr .= '<span>WS14 4JW </span><br />';
        $footerstr .= '<span>Tel: <a href="#" style="color: #0068A5;text-decoration: underline;">12345 528964 </a></span></td>';
        $footerstr .= '</tr>';
        $footerstr .= '</tbody>';
        $footerstr .= '</table>';
        $footerstr .= '</div>';
        $footerstr .= '<div style="width: 100%; max-width: 500px; margin: 0px auto;">';
        $footerstr .= '<table style="margin-top: 9px;" width="100%">';
        $footerstr .= '<tbody>';
        $footerstr .= '<tr>';
        $footerstr .= '</tr>';
        $footerstr .= '</tbody>';
        $footerstr .= '</table>';
        $footerstr .= '</div>';

        return $footerstr;
    }

    public static function MailSendGeneralFinal($fromData = array(), $subject = "", $message = "", $mailids = array(), $attachments = array())
    {
        $replyToMail = $fromData['username'];
        $replyToName = 'EducationClub Admin';

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->CharSet    = "utf-8";
        $mail->Host       = $fromData['host'];
        $mail->SMTPAuth   = true;
        // $mail->SMTPDebug  = 1;
        $mail->SMTPSecure = 'ssl';
        $mail->Port       = $fromData['port'];
        $mail->Username   = $fromData['username'];
        $mail->Password   = $fromData['password'];
        $mail->setFrom($fromData['from_email'], $fromData['from_name']);
        $mail->AddReplyTo($fromData['replay_email'], $fromData['from_name']);

        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        if (!empty($attachments)) {
            foreach ($attachments as $key => $value) {
                $mail->addAttachment($value, $key);
            }
        }

        if (!empty($mailids)) {
            foreach ($mailids as $key => $value) {
                $mail->addAddress($value, $key);
            }
        }
        // $mail->isHTML(true);
        // $mail->send();
        // echo "<pre>";
        // print_r($mail->ErrorInfo);
        // exit;

        // if($mail->send()){
        // 	echo "send";
        // }else{
        // 	echo $mail->ErrorInfo;
        // }
        // exit;
        try {
            $a = $mail->send();
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
            exit;
        }
        return $a;
    }

    public static function sendMail($sendTo, $subject, $body, $attachments = array(), $bcc = 0)
    {
        require 'vendor/autoload.php';
        $fromData = Emailtemplate::mailFromData();
        // dd($fromData);
        $mail = new PHPMailer(true);
        // dd($mail);
        try {

            if (env('MAIL_MAILER') == 'smtp') {
                //Server settings
                $mail->SMTPDebug  = 0;                                     // Enable verbose debug output
                $mail->isSMTP();                                           // Set mailer to use SMTP
                $mail->Host       = $fromData['host']; // Specify main and backup SMTP servers
                $mail->SMTPAuth   = true;                                  // Enable SMTP authentication
                $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
                $mail->Port       = $fromData['port'];
                $mail->Username   = $fromData['username'];
                $mail->Password   = $fromData['password'];

                /* added for dev 2 */
                // $mail->SMTPOptions = array(
                //     'ssl' => array(
                //         'verify_peer' => false,
                //         'verify_peer_name' => false,
                //         'allow_self_signed' => true
                //     )
                // );                    // TCP port to connect to

                //Recipients
                $mail->setFrom($fromData['from_email'], $fromData['from_name']);
                $mail->AddReplyTo($fromData['replay_email'], $fromData['from_name']);
                foreach ($sendTo as $email) {
                    if (Emailtemplate::validateEmail($email)) {
                        $mail->addAddress($email);     // Add a recipient
                    }
                }

                if ($bcc ==  1) {
                    $admins = AdminModel::orderBy('v_fname')->get();
                    if (!empty($admins) && count($admins)) {
                        foreach ($admins as $k => $admin) {
                            $u_name = $admin['v_fname'] . ' ' . $admin['v_lname'];
                            $mail->AddBCC($admin['v_email'], $u_name);
                        }
                    }
                }

                //Content
                $mail->isHTML(true); // Set email format to HTML
                $mail->Subject = $subject;
                $mail->Body    = $body;
                if (!empty($attachments)) {
                    foreach ($attachments as $key => $value) {
                        $mail->addAttachment($value, $key);
                    }
                }
                // $a = $mail->send();
                // dd($a);
                if ($mail->send()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (mailerException $e) {
            //echo'<pre>'; print_r($e->ErrorInfo); exit;
            echo 'Message could not be sent. Mailer Error: ', $e->ErrorInfo;
            exit;
            return false;
        }
    }

    public static function validateEmail($email)
    {

        $sanitized_email = filter_var(($email), FILTER_SANITIZE_EMAIL);

        if (filter_var($sanitized_email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        return false;
    }

    public static function getSiteSetting($key)
    {

        if (isset($key) && $key) {
            $return = SiteSettingsModel::where('v_key', trim($key))->value('l_value');
            return $return;
        }
        return false;
    }
}
