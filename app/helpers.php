<?php
use Request AS Request, 
	Route AS Route, 
	Session AS Session, 
	DB AS DB,
	Config AS Config,
  File AS File;
    
// function_exists('_p')


// COMMON FUNCTIONS --------------------------------------------------------------------------------------------------- //
function _p($data, $exit=0){ echo '<pre>'; print_r($data); echo '</pre>'; if($exit) die(''); }
function _val($val){
	if(is_numeric($val)) return $val;
	return $val ? $val : '-';
}
function _lvl(){ return '<span class="lvl">&nbsp; &raquo; &nbsp;</span>'; }
function siteName(){ return env('APP_NAME'); }
function uniqueCode(){ return uniqid(); }
function seoText($str){
	for($i=0; $i<5; $i++){
		$str = str_replace('&', 'and', trim( $str ));
		$str = str_replace(['#','$','\'','"','?','&',':','!','%','&reg;','&trade;','(',')','/',',','_'], '-', trim( $str ));
		$str = str_replace([' '], '-', trim( $str ));
		$str = str_replace(['--', '--'], '-', trim( $str ));
	}
	return $str;
}
function siteConfig($key, $default=NULL){ return config('site-config.'.$key, $default); }
function sSetting($key, $usable=0){
	return \SiteSetting::fetchObj($key, $usable);
}
function makeDropdown($options=[], $selected=[]){
	$return = [];
	if(count($options)){
		foreach($options as $k => $v){
			$sel = '';
			if(is_array($selected)){
				if(in_array($k, $selected)) 
					$sel = 'selected';
			}
			else if($selected === $k) {
				$sel = 'selected';
			}
			$return[] = '<option value="'.$k.'" '.$sel.'>'.ucfirst(trim(strip_tags($v))).'</option>';
		}
	}
	return implode('', $return);
}
function printPrice($price=0){ return '$'.round($price, 2); }


// TRANSLATION FUNCTIONS --------------------------------------------------------------------------------------------------- //
function getMsg($key, $args=[]){ return trans('messages.'.$key, $args); }


// DATE FUNCTIONS --------------------------------------------------------------------------------------------------- //
function dbDate(){ return date('Y-m-d H:i:s'); }
function frDate($date, $datetime=0){ return date($datetime ? 'm/d/Y H:i:s' : 'm/d/Y', strtotime($date)); }


// ADMIN FUNCTIONS --------------------------------------------------------------------------------------------------- //
function _admin($key=''){
	if( !auth()->guard('admin')->check() ) return 0;
	$user = auth()->guard('admin')->user();
	if($key == 'id') return $user->getId();
	else if($key == 'name') return $user->getName();
	return $user;
}
function adminAssets($path){ return asset('public/backend_assests/'.$path).'?ts='.time(); }
function _star(){ return '<span class="text-danger">*</span>'; }
function labelInfo($text){ return $text ? '<span class="text-info" >'.$text.'</span>' : ''; }

// ROUTE FUNCTIONS --------------------------------------------------------------------------------------------------- //
function routePut($name){ return $name && Route::has($name) ? route($name) : ''; }
function routeActive($name){ return Route::getCurrentRoute()->getName() == $name ? 1 : '';; }


// FILE FUNCTIONS --------------------------------------------------------------------------------------------------- //
function fileDriver(){ return 'uploads'; }
function fileExists($folder, $filename){ return $filename ? \Storage::disk(fileDriver())->exists($folder.'/'.$filename) : ''; }
function fileUpload($folder, $file, $filename='', $replace=0){
	if($file){
		$filename = $filename ?: seoText($file->getClientOriginalName());
		if(!$replace){
			if(fileExists($folder, $filename))
				$filename = pathinfo($filename, PATHINFO_FILENAME).'-'.uniqueCode().'.'.pathinfo($filename, PATHINFO_EXTENSION);
		}
		if(\Storage::disk(fileDriver())->put($folder.'/'.$filename, file_get_contents($file)))
			return $filename;
	}
	return '';
}
function fileDelete($folder, $filename){ return fileExists($folder, $filename) ? Storage::disk(fileDriver())->delete($folder.'/'.$filename) : ''; }
function fileUrl($folder, $filename){ return fileExists($folder, $filename) ? Storage::disk(fileDriver())->url($folder.'/'.$filename) : ''; }
function filePreview($url){ return $url ? '<a href="'.$url.'" target="_blank" class="ml-2" title="Preview File" ><button type="button" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></button></a>' : ''; }
function fileExtention($url){ return pathinfo($url, PATHINFO_EXTENSION); }
function fileIsImage($url){ return in_array(strtolower(fileExtention($url)), ['jpg', 'jpeg', 'png', 'gif']); }
function fileHugePreview($url, $width=50){
	if(!fileExtention($url)) return '';
	
	if(fileIsImage($url)) return '<a href="'.$url.'" target="_blank" title="Preview File" ><img src="'.$url.'" width="'.$width.'" /></a>';
	else return '<a href="'.$url.'" target="_blank" title="Preview File" ><button type="button" class="btn btn-info btn-xs"><i class="far fa-file"></i></button></a>';
}
function moveFile($old, $new){ return File::move($old, $new); }
function copyFile($old, $new){ return File::copy($old, $new); }


// HTML FUNCTIONS --------------------------------------------------------------------------------------------------- //
function cardClasses(){ return 'card card-info card-outline'; }
function pError($ele){ return 'card card-info card-outline'; }

function cardTools($arr=[]){
	return '<div class="card-tools">'.
		implode('', $arr).
		'<button type="button" class="btn btn-tool ml2" data-card-widget="collapse"><i class="fas fa-minus"></i></button>'.
		//'<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>'.
	'</div>';
}


// DATATABLE FUNCTIONS --------------------------------------------------------------------------------------------------- //
function dtblStart($default=0){ return (int)request('start', $default); }
function dtblLimit($default=10){ return (int)request('length', $default); }
function dtblSortBy($args, $default){ return $args[request('columns.'.request('order.0.column').'.data', $default)] ?? $default; }
function dtblSortDir($default='ASC'){ return request('order.0.dir', $default); }
function dtblSearch(){ return request('search.value') ?: request('srch_general'); }
function dtblMultiCheck(){ return '<input type="checkbox" class="crud-check" onchange="crudMultiCheck(this)" />'; }
function dtblMultiActive($url){ return '<a href="javascript:void(0);" onclick="crudStatusChange(\''.$url.'\', 1)" ><button type="button" class="btn btn-secondary btn-sm">Active</button></a>'; }
function dtblMultiInactive($url){ return '<a href="javascript:void(0);" onclick="crudStatusChange(\''.$url.'\', 0)" ><button type="button" class="btn btn-secondary btn-sm">Inactive</button></a>'; }
function dtblMultiDelete($url){ return '<a href="javascript:void(0);" onclick="crudDelete(\''.$url.'\')" ><button type="button" class="btn btn-danger btn-sm">Delete</button></a>'; }
function dtblMultiOrderUpdate($url){ return '<a href="javascript:void(0);" onclick="crudUpdateOrder(\''.$url.'\')" ><button type="button" class="btn btn-secondary btn-sm">Update Order</button></a>'; }
function dtblAdd($url){ return '<a href="'.$url.'" ><button type="button" class="btn btn-info btn-sm ml-3">Add New</button></a>'; }
function dtblWhNumeric($q, $key, $value){
	if(is_numeric($value)) $q = $q->where($key, $value);
	return $q;
}

// REQUEST FUNCTIONS --------------------------------------------------------------------------------------------------- //
function isReq($key){ return request()->exists($key); }
function isPost($request=NULL){ $request = !is_null($request) ? $request : request(); return request()->isMethod('POST') ? 1 : 0; }
function addReq($key, $val){ request()->merge([$key => $val]); }


// FRONT USER FUNCTIONS --------------------------------------------------------------------------------------------------- //
function _user($key=''){
	if(!auth()->guard('user')->check()) return 0;
	$user = auth()->guard('user')->user();
	if($key == 'id') return $user->getId();
	else if($key == 'name') return $user->getName();
	return $user;
}
function logId(){ return _user('id'); }

// FRONT FUNCTIONS --------------------------------------------------------------------------------------------------- //
function frontAssets($path){ return asset('public/'.$path).'?ts='.time(); }

// OTHER FUNCTIONS --------------------------------------------------------------------------------------------------- //
function _curl( $url, $fields = array(), $method = 'GET' ){ 
    if( $method == 'GET' ){
        try{
            if( count( $fields ) ) $url = $url.'?'.http_build_query( $fields );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url ); 
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_POST, 0 ); 
            curl_setopt($ch, CURLOPT_FAILONERROR, 1 ); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt($ch, CURLOPT_TIMEOUT, 10000 ); 
            $retValue = curl_exec($ch);
            curl_close($ch);
        }
        catch( Exception $e ){
        }
    }
    else{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url ); 
        curl_setopt($ch, CURLOPT_POST, 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $fields ) );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false ); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000 ); 
        $retValue = curl_exec($ch);
        curl_close($ch);
    }
    return $retValue; 
}

