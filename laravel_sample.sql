-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2020 at 10:38 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_sample`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) UNSIGNED NOT NULL,
  `v_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ti_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1: Active, 0: Inactive',
  `i_order` int(11) NOT NULL DEFAULT 0,
  `d_added` datetime DEFAULT NULL,
  `d_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `v_slug`, `v_name`, `l_description`, `v_image`, `ti_status`, `i_order`, `d_added`, `d_updated`) VALUES
(6, 'category-1', 'Category 1', '<p>Category 1<br></p>', 'jpg-50kb.jpg', 1, 1, '2020-08-11 12:15:15', '2020-08-31 07:10:09'),
(7, 'category-2', 'Category 2', NULL, NULL, 1, 2, '2020-08-11 12:15:39', '2020-08-31 07:11:00'),
(8, 'category-9', 'Category 9', NULL, NULL, 1, 9, '2020-08-11 19:10:36', '2020-08-31 07:12:35'),
(9, 'category-11', 'Category 11', NULL, NULL, 1, 11, '2020-08-11 20:13:42', '2020-08-31 07:12:52'),
(10, 'category-3', 'Category 3', NULL, NULL, 1, 3, '2020-08-14 16:32:15', '2020-08-31 07:11:11'),
(11, 'category-10', 'Category 10', NULL, NULL, 1, 10, '2020-08-14 16:32:38', '2020-08-31 07:12:44'),
(12, 'category-4', 'Category 4', NULL, NULL, 1, 4, '2020-08-14 16:42:25', '2020-08-31 07:11:23'),
(13, 'category-5', 'Category 5', NULL, NULL, 1, 5, '2020-08-14 16:46:04', '2020-08-31 07:11:31'),
(14, 'category-6', 'Category 6', NULL, NULL, 1, 6, '2020-08-14 16:46:36', '2020-08-31 07:11:39'),
(15, 'category-7', 'Category 7', NULL, NULL, 1, 7, '2020-08-14 16:47:04', '2020-08-31 07:11:48'),
(16, 'category-12', 'Category 12', NULL, NULL, 1, 12, '2020-08-23 19:31:09', '2020-08-31 07:13:01'),
(17, 'category-14', 'Category 14', NULL, NULL, 1, 14, '2020-08-23 19:31:39', '2020-08-31 07:13:14'),
(18, 'category-15', 'Category 15', NULL, NULL, 1, 15, '2020-08-23 19:32:09', '2020-08-31 07:13:18'),
(21, 'category-8', 'Category 8', NULL, NULL, 1, 8, '2020-08-24 13:33:52', '2020-08-31 07:12:26'),
(22, 'category-13', 'Category 13', NULL, NULL, 1, 13, '2020-08-24 13:34:14', '2020-08-31 07:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `v_first_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_last_name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ti_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1: Active, 0: Inactive',
  `d_added` datetime DEFAULT NULL,
  `d_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `v_first_name`, `v_last_name`, `v_email`, `v_phone`, `v_password`, `ti_status`, `d_added`, `d_updated`) VALUES
(1, 'Deven', 'R', 'deven.ahir13@gmail.com', '8866207256', '$2y$10$MFOeAbDjIHwZeAWttiId5OSucXrtS8rWotGtcDyDL2r96DmUHFfs.', 1, '2020-08-11 10:12:19', '2020-08-11 10:25:23'),
(2, 'Eli', 'test', 'eli@webcodeny.com', '3475428335', NULL, 1, '2020-08-14 10:48:34', '2020-08-14 10:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_media`
--

CREATE TABLE `tbl_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `v_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `id` int(11) UNSIGNED NOT NULL,
  `v_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ti_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1: Active, 0: Inactive',
  `d_added` datetime DEFAULT NULL,
  `d_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_page`
--

INSERT INTO `tbl_page` (`id`, `v_slug`, `v_name`, `l_description`, `v_image`, `ti_status`, `d_added`, `d_updated`) VALUES
(2, 'our-story', 'Our Story', NULL, NULL, 1, '2020-08-11 12:14:21', '2020-08-11 12:14:21'),
(3, 'contact-us', 'Contact Us', NULL, NULL, 1, '2020-08-16 00:49:39', '2020-08-16 00:49:39'),
(4, 'privacy-policy', 'Privacy Policy', NULL, NULL, 1, '2020-08-23 19:08:10', '2020-08-23 19:08:10'),
(5, 'terms-and-conditions', 'Terms & Conditions', NULL, NULL, 1, '2020-08-23 19:08:32', '2020-08-23 19:08:32'),
(6, 'delivery-information', 'Delivery Information', '<p>Coming soon.</p>', NULL, 1, '2020-08-23 19:09:17', '2020-08-23 19:09:17'),
(7, 'about', 'About', '<p>Coming Soon.</p>', NULL, 1, '2020-08-23 19:38:42', '2020-08-23 19:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) UNSIGNED NOT NULL,
  `v_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_sku` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_price` float(10,2) NOT NULL,
  `f_sale_price` float(10,2) NOT NULL,
  `i_qty` int(11) NOT NULL DEFAULT 0,
  `ti_new_arrival` tinyint(4) NOT NULL DEFAULT 0,
  `ti_status` tinyint(4) NOT NULL DEFAULT 1,
  `i_order` int(11) NOT NULL DEFAULT 0,
  `d_added` datetime DEFAULT NULL,
  `d_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `v_slug`, `v_name`, `l_description`, `v_sku`, `f_price`, `f_sale_price`, `i_qty`, `ti_new_arrival`, `ti_status`, `i_order`, `d_added`, `d_updated`) VALUES
(1, 'product-1', 'Product 1', NULL, 'Product 1', 100.00, 90.00, 50, 1, 1, 1, '2020-08-17 11:51:51', '2020-08-22 05:51:18'),
(2, 'product-3', 'Product 3', '<p>Product 3<br></p>', 'Product 3', 190.00, 0.00, 10, 0, 1, 3, '2020-08-22 22:05:01', '2020-08-31 07:26:37'),
(3, 'product-2', 'Product 2', '<p>Product 2<br></p>', 'Product 2', 210.00, 160.00, 0, 0, 1, 2, '2020-08-22 22:52:12', '2020-08-31 07:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_relation`
--

CREATE TABLE `tbl_product_relation` (
  `i_product_id` int(11) NOT NULL,
  `ti_type` tinyint(4) NOT NULL DEFAULT 0,
  `i_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_product_relation`
--

INSERT INTO `tbl_product_relation` (`i_product_id`, `ti_type`, `i_type_id`) VALUES
(1, 1, 6),
(1, 1, 7),
(1, 1, 10),
(2, 1, 6),
(2, 1, 9),
(3, 1, 6),
(3, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_site_setting`
--

CREATE TABLE `tbl_site_setting` (
  `id` int(11) NOT NULL,
  `v_key` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_site_setting`
--

INSERT INTO `tbl_site_setting` (`id`, `v_key`, `l_value`) VALUES
(1, 'logo', 'logo2.png'),
(2, 'favicon', 'square-qube.jpg'),
(3, 'google_analytics', '<script></script>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE `tbl_state` (
  `id` int(11) UNSIGNED NOT NULL,
  `i_country_id` int(11) NOT NULL DEFAULT 0,
  `v_code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ti_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1: Active, 0: Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_state`
--

INSERT INTO `tbl_state` (`id`, `i_country_id`, `v_code`, `v_name`, `ti_status`) VALUES
(5, 1, 'CA', 'California', 1),
(9, 1, 'IA', 'Iowa', 1),
(10, 1, 'NY', 'New York', 1),
(14, 1, 'NJ', 'New Jersey', 1),
(15, 1, 'MA', 'Massachusetts', 1),
(16, 1, 'CT', 'Connecticut', 1),
(19, 1, 'FL', 'Florida', 1),
(24, 1, 'TX', 'Texas', 1),
(28, 1, 'AA', 'Armed Forces US', 1),
(32, 1, 'TN', 'Tennessee', 1),
(33, 1, 'KY', 'Kentucky', 1),
(35, 1, 'GA', 'Georgia', 1),
(44, 1, 'IL', 'Illinois', 1),
(47, 1, 'CO', 'Colorado', 1),
(49, 1, 'UT', 'Utah', 1),
(57, 1, 'MD', 'Maryland', 1),
(72, 1, 'SC', 'South Carolina', 1),
(73, 1, 'MT', 'Montana', 1),
(76, 1, 'LA', 'Louisiana', 1),
(84, 1, 'WA', 'Washington', 1),
(87, 1, 'PA', 'Pennsylvania', 1),
(93, 1, 'NC', 'North Carolina', 1),
(98, 1, 'MI', 'Michigan', 1),
(101, 1, 'AR', 'Arkansas', 1),
(107, 1, 'WI', 'Wisconsin', 1),
(129, 1, 'OH', 'Ohio', 1),
(132, 1, 'NM', 'New Mexico', 1),
(133, 1, 'KS', 'Kansas', 1),
(136, 1, 'OR', 'Oregon', 1),
(140, 1, 'NE', 'Nebraska', 1),
(143, 1, 'WV', 'West Virginia', 1),
(144, 1, 'VA', 'Virginia', 1),
(145, 1, 'MO', 'Missouri', 1),
(148, 1, 'MS', 'Mississippi', 1),
(151, 1, 'RI', 'Rhode Island', 1),
(163, 1, 'IN', 'Indiana', 1),
(164, 1, 'OK', 'Oklahoma', 1),
(175, 1, 'MN', 'Minnesota', 1),
(176, 1, 'AL', 'Alabama', 1),
(181, 1, 'AZ', 'Arizona', 1),
(186, 1, 'SD', 'South Dakota', 1),
(192, 1, 'NV', 'Nevada', 1),
(215, 1, 'NH', 'New Hampshire', 1),
(218, 1, 'ME', 'Maine', 1),
(220, 1, 'HI', 'Hawaii', 1),
(249, 1, 'DC', 'District of Columbia', 1),
(254, 1, 'DE', 'Delaware', 1),
(305, 1, 'ID', 'Idaho', 1),
(667, 1, 'WY', 'Wyoming', 1),
(924, 1, 'ND', 'North Dakota', 1),
(959, 1, 'VT', 'Vermont', 1),
(1061, 1, 'AK', 'Alaska', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ti_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1: Active, 0: Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `ti_status`, `created_at`, `updated_at`) VALUES
(1, 'Master Admin', 'admin@admin.com', '2020-07-31 16:41:06', '$2b$10$lA49eK7PINvJuh5E5zwVwuP1C2vilwCJqvJkAwudcxfHvFh8b8X4a', 'cJQmUvAbQOjzjtvvroRkVlJnAulDZjTKcu97o6Lx3dPYQjR780n7TBzwZUoL', 1, '2020-07-31 16:41:06', '2020-07-31 16:41:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `i_order` (`i_order`),
  ADD KEY `v_slug` (`v_slug`),
  ADD KEY `ti_status` (`ti_status`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`v_email`),
  ADD KEY `ti_status` (`ti_status`);

--
-- Indexes for table `tbl_media`
--
ALTER TABLE `tbl_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `v_slug` (`v_slug`),
  ADD KEY `ti_status` (`ti_status`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `i_order` (`i_order`),
  ADD KEY `v_slug` (`v_slug`),
  ADD KEY `v_sku` (`v_sku`),
  ADD KEY `f_sale_price` (`f_sale_price`),
  ADD KEY `f_price` (`f_price`),
  ADD KEY `ti_status` (`ti_status`);

--
-- Indexes for table `tbl_product_relation`
--
ALTER TABLE `tbl_product_relation`
  ADD KEY `i_product_id` (`i_product_id`),
  ADD KEY `i_type_id` (`i_type_id`),
  ADD KEY `ti_type` (`ti_type`);

--
-- Indexes for table `tbl_site_setting`
--
ALTER TABLE `tbl_site_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `v_key` (`v_key`);

--
-- Indexes for table `tbl_state`
--
ALTER TABLE `tbl_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_icountryid_index` (`i_country_id`),
  ADD KEY `ti_status` (`ti_status`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `ti_status` (`ti_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_media`
--
ALTER TABLE `tbl_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_site_setting`
--
ALTER TABLE `tbl_site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_state`
--
ALTER TABLE `tbl_state`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1062;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
