# Laravel Demo by Deven

## Versions
- Laravel Version 7.26.1
- PHP Version Minimum 7.2.5

## Admin Info
- Admin is developed with **[AdminLTE-3](https://adminlte.io/themes/v3/index.html)**.
- Database sample file is in root folder as **laravel_sample.sql**

## Some Basic Rules
- Always make routes with name.
- Try to use traits whenever possible.
- Always use common functions.
- Always try to make reusable code.
- Always use laravel relationships.
- Minimum use of DB class.
- Maximum use of model base query.
- add/edit/delete boot events on model