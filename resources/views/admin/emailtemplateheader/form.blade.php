@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="{{ cardClasses() }}">
                <div class="card-header">
                    <h3 class="card-title">{{ $single->getId() ? 'Edit Record' : 'Add Record' }}</h3>
                    {!! cardTools() !!}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Title {!! _star()!!}</label>
                                <input type="text" class="form-control" name="v_title" value="{!! $single->v_title !!}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description{!! _star()!!}</label>
                                <textarea class="form-control txteditor" name="l_description" required>{!! $single->getDescription() !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Status {!! _star()!!}</label>
                                <select class="form-control select2" name="ti_status" required data-parsley-errors-container=".err-estatus">
                                    <option value=""></option>
                                    {!! makeDropdown(siteConfig('ti_status'), $single->getStatus()) !!}
                                </select>
                                <div class="err-estatus"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">SUBMIT</button>
                    <a href="{{$urlList}}"><button type="button" class="btn btn-default ml-2">Cancel</button></a>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection
@push('jsfun')
<script type="text/javascript">
   
</script>
@endpush