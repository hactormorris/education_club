@extends('admin.layouts.app')
@section('title','Site Settings Management')
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="{{ cardClasses() }}">
      <div class="card-header">
        <h3 class="card-title">Site Settings</h3>
        {!! cardTools() !!}
      </div>
      <div class="card-body">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active" style="font-size:20px;margin-right:20px;"><a href="#tab_1" data-toggle="tab" class="active">General Settings</a></li>
            <li style="font-size:20px;"><a href="#tab_2" data-toggle="tab"> SMTP Settings For Email</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
              <br />
              <form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{route('admin.sitesetting.store')}}" method="POST" data-parsley-validate>
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Site Name</label>
                      <input type="text" name="SITE_NAME" id="SITE_NAME" class="form-control" placeholder="Site Name" value="{{ isset($data['SITE_NAME']) ? $data['SITE_NAME'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <label>Address</label>
                      <textarea name="SITE_ADDRESS" id="SITE_ADDRESS" class="form-control" placeholder="Address" rows="5" required data-parsley-trigger="keyup">{{ isset($data['SITE_ADDRESS']) ? $data['SITE_ADDRESS'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                      <div><label>Site Logo</label></div>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="background: black;width: 200px;">
                          @if(isset($data['SITE_LOGO']) && $data['SITE_LOGO'] != '')
                          <img src="http://192.168.0.165/projects/education_club/storage/app/public/{{$data['SITE_LOGO']}}" alt= />
                          @else
                          <img src="{{ $_S3_PATH ?? '' }}/common/noimage.png" alt= />
                          @endif
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                        <div>
                          <span class="btn default btn-file">
                            <span class="fileinput-exists"> Change Site Logo</span>
                            <input type="file" name="SITE_LOGO"> </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    {{-- site -email  --}}
                    <div class="form-group">
                      <label>Site Email</label>
                      <input type="text" name="SITE_EMAIL" id="SITE_EMAIL" class="form-control" placeholder="Site Email" value="{{ isset($data['SITE_EMAIL']) ? $data['SITE_EMAIL'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <label>Site Footer</label>
                      <input type="text" name="SITE_FOOTER_TEXT" id="SITE_FOOTER_TEXT" class="form-control" value="{{ isset($data['SITE_FOOTER_TEXT']) ? $data['SITE_FOOTER_TEXT'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                    <div class="form-group">
                      <div><label>Site Favicon Logo</label></div>
                      <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                          @if(isset($data['SITE_FAVICON_LOGO']) && $data['SITE_FAVICON_LOGO'] != '')
                          <img src="http://192.168.0.165/projects/education_club/storage/app/public/{{$data['SITE_FAVICON_LOGO']}}" alt= />
                          @else
                          <img src="{{url('public/images/favicon.ico')}}" alt= />
                          @endif
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                        <div>
                          <span class="btn default btn-file">
                            <span class="fileinput-exists"> Change Site Favicon Logo</span>
                            <input type="file" name="SITE_FAVICON_LOGO"> </span>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <a href="{{route('admin.sitesetting.index')}}">
                  <button type="button" class="btn btn-warning">Cancel</button>
                </a>
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
              <br />
            </div>

            <div class="tab-pane" id="tab_2">
              <br />
              <form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{route('admin.sitesetting.store')}}" method="POST" data-parsley-validate>
                {{ csrf_field() }}

                <div class="row">
                  <div class="col-md-3">
                    {{-- SMTP HOST  --}}
                    <div class="form-group">
                      <label>SMTP Host</label>
                      <input type="text" name="SMTP_HOST" id="SMTP_HOST" class="form-control" placeholder="SMTP Host" value="{{isset($data['SMTP_HOST']) ? $data['SMTP_HOST'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                    {{-- SMTP FROM EMAIL  --}}
                    <div class="form-group">
                      <label>SMTP From Email</label>
                      <input type="email" name="SMTP_FROM_EMAIL" id="SMTP_FROM_EMAIL" class="form-control" placeholder="SMTP From Email" value="{{isset($data['SMTP_FROM_EMAIL']) ? $data['SMTP_FROM_EMAIL'] : ''}}" required data-parsley-trigger="keyup">
                    </div>

                  </div>
                  <div class="col-md-3">

                    {{-- SMTP USERNAME  --}}
                    <div class="form-group">
                      <label>SMTP Username</label>
                      <input type="text" name="SMTP_USERNAME" id="SMTP_USERNAME" class="form-control" placeholder="SMTP Username" value="{{isset($data['SMTP_USERNAME']) ? $data['SMTP_USERNAME'] : ''}}" required data-parsley-trigger="keyup">
                    </div>

                    {{-- SMTP PORT  --}}
                    <div class="form-group">
                      <label>SMTP Port</label>
                      <input type="text" name="SMTP_PORT" id="SMTP_PORT" class="form-control" placeholder="SMTP Port" value="{{isset($data['SMTP_PORT']) ? $data['SMTP_PORT'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                  </div>

                  <div class="col-md-3">

                    {{-- SMTP PROTOCOL  --}}
                    <div class="form-group">
                      <label>SMTP Protocol</label>
                      <input type="text" name="SMTP_PROTOCOL" id="SMTP_PROTOCOL" class="form-control" placeholder="SMTP Protocol" value="{{isset($data['SMTP_PROTOCOL']) ? $data['SMTP_PROTOCOL'] : ''}}" required data-parsley-trigger="keyup">
                    </div>

                    {{-- SMTP FROM NAME  --}}
                    <div class="form-group">
                      <label>SMTP From Name </label>
                      <input type="text" name="SMTP_FROM_NAME" id="SMTP_FROM_NAME" class="form-control" placeholder="SMTP From Name" value="{{isset($data['SMTP_FROM_NAME']) ? $data['SMTP_FROM_NAME'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                  </div>
                  <div class="col-md-3">
                    {{-- SMTP PASSWORD  --}}
                    <div class="form-group">
                      <label>SMTP Password</label>
                      <input type="password" name="SMTP_PASSWORD" id="SMTP_PASSWORD" class="form-control" placeholder="SMTP Password" value="{{isset($data['SMTP_PASSWORD']) ? $data['SMTP_PASSWORD'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                    {{-- SMTP REPLY EMAIL  --}}
                    <div class="form-group">
                      <label>SMTP Reply Email </label>
                      <input type="text" name="SMTP_REPLY_EMAIL" id="SMTP_REPLY_EMAIL" class="form-control" placeholder="SMTP From Name" value="{{isset($data['SMTP_REPLY_EMAIL']) ? $data['SMTP_REPLY_EMAIL'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                  </div>
                </div>

                <a href="{{route('admin.sitesetting.index')}}">
                  <button type="button" class="btn btn-warning">Cancel</button>
                </a>
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
              <form class="horizontal-form" action="{{route('admin.sitesetting.store')}}" name="employment-edit" id="employment-edit" enctype="multipart/form-data" method="POST" data-parsley-validate>
                {{ csrf_field() }}

                <div class="row" style="margin-top: 20px;">
                  <div class="col-md-6">
                    {{-- SEND TEST EMAIL  --}}
                    <div class="form-group">
                      <label>SEND TEST EMAIL</label>
                      <input type="email" name="SEND_TEST_EMAIL" id="SEND_TEST_EMAIL" class="form-control" placeholder="SEND TEST EMAIL" value="{{isset($data['SEND_TEST_EMAIL']) ? $data['SEND_TEST_EMAIL'] : ''}}" required data-parsley-trigger="keyup">
                    </div>
                  </div>
                  <div class="col-md-6">

                    {{-- MESSAGE  --}}
                    <div class="form-group">
                      <label>MESSAGE</label>
                      <textarea name="SITE_MESSAGE" id="SITE_MESSAGE" class="form-control" placeholder="MESSAGE" rows="3" required data-parsley-trigger="keyup">{{isset($data['SITE_MESSAGE']) ? $data['SITE_MESSAGE'] : ''}}</textarea>
                    </div>
                  </div>
                </div>
                <a href="{{route('admin.sitesetting.index')}}">
                  <button type="button" class="btn btn-warning">Cancel</button>
                </a>
                <button type="submit" class="btn btn-primary">Send mail</button>
                <br /><br />
              </form>


            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection