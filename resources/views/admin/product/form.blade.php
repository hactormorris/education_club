@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="{{ cardClasses() }}">
        <div class="card-header">
          <h3 class="card-title">{{ $single->getId() ? 'Edit Record' : 'Add Record' }}</h3>
          {!! cardTools() !!}
        </div>
        <div class="card-body">
        	<div class="row">
          	<div class="col-md-4">
              <div class="form-group">
                <label>Name {!! _star()!!}</label>
                <input type="text" class="form-control" name="v_name" value="{!! $single->getName() !!}" required >
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Product SKU {!! _star()!!}</label>
                <input type="text" class="form-control" id="v_sku" name="v_sku" value="{!! $single->getSku() !!}" required onblur="checkUnique('v_sku')" >
                <div id="uniqueInfoSku" ></div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Slug [Canonical URL] {!! _star()!!}</label>
                <input type="text" class="form-control" id="v_slug" name="v_slug" value="{!! $single->getSlug() !!}" required onblur="checkUnique('v_slug')" >
                <div id="uniqueInfo" ></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Order {!! _star()!!}</label>
                <input type="number" class="form-control" name="i_order" value="{!! $single->getOrder() !!}" required >
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Status {!! _star()!!}</label>
                <select class="form-control select2" name="ti_status" required data-parsley-errors-container=".err-estatus" >
                	<option value="" ></option>
                	{!! makeDropdown(siteConfig('ti_status'), $single->getStatus()) !!}
                </select>
                <div class="err-estatus" ></div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control txteditor" name="l_description" >{!! $single->getDescription() !!}</textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info">SUBMIT</button>
          <a href="{{$urlList}}" ><button type="button" class="btn btn-default ml-2" >Cancel</button></a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="{{ cardClasses() }}">
        <div class="card-header">
          <h3 class="card-title">Prices / Stock</h3>
          {!! cardTools() !!}
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Price {!! _star()!!}</label>
                <input type="number" class="form-control" name="f_price" value="{!! $single->getPrice() !!}" required />
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Sale Price</label>
                <input type="number" class="form-control" name="f_sale_price" value="{!! $single->getSalePrice() !!}" />
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Stock</label>
                <input type="number" class="form-control" name="i_qty" value="{!! $single->getQty() !!}" />
              </div>
            </div>
            <div class="col-md-3">
            	<div class="form-group row">
                <label class="col-md-12">New Arrival</label>
                <div class="col-md-12">
                	<div class="icheck-primary d-inline">
                    <input type="checkbox" id="ti_new_arrival" name="ti_new_arrival" value="1" {{ $single->isNewArrival() ? 'checked' : '' }} />
                    <label for="ti_new_arrival">Check if yes</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info">SUBMIT</button>
          <a href="{{$urlList}}" ><button type="button" class="btn btn-default ml-2" >Cancel</button></a>
        </div>
      </div>
    </div>
  </div>
      
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="{{ cardClasses() }}">
        <div class="card-header">
          <h3 class="card-title">Product Category</h3>
          {!! cardTools() !!}
        </div>
        <div class="card-body">
        	<div class="row">
          	<div class="col-12">
              <div class="form-group">
                <label>Product Category</label>
                <select class="duallistbox" multiple="multiple" name="category_ids[]" >
                	{!! makeDropdown(\Category::dropdown(), $single->getCategoryIds()) !!}
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info">SUBMIT</button>
          <a href="{{$urlList}}" ><button type="button" class="btn btn-default ml-2" >Cancel</button></a>
        </div>
      </div>
    </div>
  </div>
</form>

@endsection
@push('jsfun')
<script type="text/javascript" >
	var urlCheckUnique = '{{ $urlCheckUnique }}';
</script>
@endpush
