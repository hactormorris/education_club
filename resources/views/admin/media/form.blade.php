@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="{{ cardClasses() }}">
        <div class="card-header">
          <h3 class="card-title">Upload</h3>
          {!! cardTools() !!}
        </div>
        <div class="card-body">
        	<div class="row">
          	<div class="col-md-12">
              <div class="form-group">
                <label>Upload File {!! _star()!!}</label>
                <input type="file" class="form-control" name="v_file" required />
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info">SUBMIT</button>
          <a href="{{$urlList}}" ><button type="button" class="btn btn-default ml-2" >Cancel</button></a>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection