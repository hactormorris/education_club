@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="{{ cardClasses() }}">
        <div class="card-header">
          <h3 class="card-title">{{ $single->getId() ? 'Edit Record' : 'Add Record' }}</h3>
          {!! cardTools() !!}
        </div>
        <div class="card-body">
          <div class="box-header with-border">
            <h3 class="box-title-custom">Page Detail</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Page Title {!! _star()!!}</label>
                  <input type="text" class="form-control" name="v_title" value="{!! $single->v_title !!}" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Page Slug [Canonical URL] {!! _star()!!}</label>
                  <input type="text" class="form-control" id="v_slug" name="v_slug" value="{!! $single->v_slug !!}" required onblur="checkUnique('v_slug')">
                  <div id="uniqueInfo"></div>
                </div>
              </div>
            </div>
          </div>



          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Page Header Title {!! _star()!!}</label>
                <input type="text" class="form-control" name="v_header_title" value="{!! $single->v_header_title !!}" required>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Status {!! _star()!!}</label>
                <select class="form-control select2" name="ti_status" required data-parsley-errors-container=".err-estatus">
                  <option value=""></option>
                  {!! makeDropdown(siteConfig('ti_status'), $single->getStatus()) !!}
                </select>
                <div class="err-estatus"></div>
              </div>
            </div>
          </div>




          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title-custom">Page Meta Detail</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Meta Title {!! _star()!!}</label>
                    <input type="text" class="form-control" name="v_meta_title" value="{!! $single->v_meta_title !!}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Meta Keyword {!! _star()!!}</label>
                    <input type="text" class="form-control" id="v_meta_keywords" name="v_meta_keywords" value="{!! $single->v_meta_keywords !!}" required onblur="checkUnique('v_slug')">
                    <div id="uniqueInfo"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>



          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Meta Description{!! _star()!!}</label>
                <textarea class="form-control" name="l_meta_description" required>{!! $single->l_meta_description !!}</textarea>
              </div>
            </div>
          </div>



          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Other Meta{!! _star()!!}</label>
                <textarea class="form-control" name="l_other_meta" required>{!! $single->l_other_meta !!}</textarea>
              </div>
            </div>
          </div>
  


          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title-custom">Page Content</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Description{!! _star()!!}</label>
                    <textarea class="form-control txteditor" name="l_description" required>{!! $single->l_description !!}</textarea>
                  </div>
                </div>
              </div>



              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Page Header Image</label>
                    <input type="file" name="v_image" class="form-control" {{ $single->getId() ? '' : 'required'}} />
                    @if($single->getId())
                    <div class="mt-2"><img src={{URL::asset('public/uploads/page/'.$single->v_image)}}></div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info">SUBMIT</button>
          <a href="{{$urlList}}"><button type="button" class="btn btn-default ml-2">Cancel</button></a>
        </div>
      </div>
    </div>
  </div>

</form>
@endsection
@push('jsfun')
<script type="text/javascript">
  var urlCheckUnique = '{{ $urlCheckUnique }}';
</script>
@endpush