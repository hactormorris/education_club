@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="{{ cardClasses() }}">
                <div class="card-header">
                    <h3 class="card-title">{{ $single->getId() ? 'Edit Record' : 'Add Record' }}</h3>
                    {!! cardTools() !!}
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Title {!! _star()!!}</label>
                                <input type="text" class="form-control" name="v_title" value="{!! $single->v_title !!}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Topic {!! _star()!!}</label>
                                <select class="form-control select2" name="i_topic_id" required>
                                    <option value=""></option>
                                    {!! makeDropdown(\Topic::dropdown(),$single->i_topic_id) !!}
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Brief Introduction Video{!! _star()!!}</label>
                                <input type="url" class="form-control" name="v_brief_introduction_video" value="{!! $single->v_brief_introduction_video !!}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Question Type {!! _star()!!}</label>
                                <select class="form-control select2" name="i_question_type" id="i_question_type" required {{ $single->getId() ? 'disabled' : ''}}>
                                    <option value=""></option>
                                    {!! makeDropdown(siteConfig('i_question_type'), $single->i_question_type) !!}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                              <div class="form-group">
                                <label>Description{!! _star()!!}</label>
                                <textarea class="form-control txteditor" name="l_description" required>{!! $single->l_description !!}</textarea>
                              </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status {!! _star()!!}</label>
                                <select class="form-control select2" name="ti_status" required data-parsley-errors-container=".err-estatus">
                                    <option value=""></option>
                                    {!! makeDropdown(siteConfig('ti_status'), $single->getStatus()) !!}
                                </select>
                                <div class="err-estatus"></div>
                            </div>
                        </div>
                    </div>




                @if(!empty($single->que_ans) && $single->i_question_type==1 && $single->que_ans->count())
                <div class="">
                    <h2 align="center">Exam Type : Fill In The Blanks</h2>
                    <div class="form-group">
                        <form action="" method="POST">
                            <table class="table table-bordered" id="dynamicAddRemove">
                               <tr><th>Questions</th><th>Answers</th><th></</tr>
                                @foreach($single->que_ans as $val)
                                @php
                                $uCode=uniqueCode();
                                @endphp
                                <tr>
                                    <input type="hidden" name="moreFields[{{$uCode}}][id]" value="{!! $val->id !!}" />
                                    <td><input type="text" name="moreFields[{{$uCode}}][question]" value="{!! $val->v_question !!}" placeholder="Enter Question" class="form-control" required/></td>
                                    <td><input type="text" name="moreFields[{{$uCode}}][answer]" value="{!! $val->v_answer !!}" placeholder="Enter Answer" class="form-control" required/></td>
                                    <td><a href="javascript:void(0);" title="Delete" onclick="DeleteQueAns(this,'{{route('admin.lesson.delete-que-ans',$val->id)}}',2)"><button type="button" class="btn btn-danger">Remove</button></a></td>
                                </tr>
                                @endforeach
                                <td colspan="3"><button type="button" name="add" id="add-btn" class="btn btn-success">Add More</button></td>
                            </table>

                        </form>
                    </div>
                </div>
                @else

                <div class="container">
                    <h2 align="center">Exam Type : Fill In The Blanks</h2>
                    <div class="form-group">
                        <form action="" method="POST">
                            <table class="table table-bordered" id="dynamicAddRemove">
                            <tr><th>Questions</th><th>Answers</th><th></</tr>
                                <tr>
                                    <input type="hidden" name="moreFields[0][id]" value="0"/>
                                    <td><input type="text" name="moreFields[0][question]" placeholder="Enter Question" class="form-control" /></td>
                                    <td><input type="text" name="moreFields[0][answer]" placeholder="Enter Answer" class="form-control" /></td>
                                    <td><button type="button" name="add" id="add-btn" class="btn btn-success">Add More</button></td>
                                </tr>
                            </table>

                        </form>
                    </div>
                </div>
                @endif




                @if(!empty($single->que_ans) && $single->i_question_type==2 && $single->que_ans->count())
                <div class="">
                    <h2 align="center" class="mb-4">Exam Type : Multiple Choice Questions</h2>
                    <div class="row" id="">
                      <div class="main-div-class">
                            <div class="col-md-4">
                                <label>Question</label>
                            </div>
                            <div class="col-md-4 add-opts">
                                <div class="row">
                                     <div class="col-md-6">
                                            <label>Options</label>
                                    </div>
                                     <div class="col-md-6">
                                        <label>Add Options</label>
                                     </div>
                                </div>
                            </div>


                             <div class="col-md-2">
                                 <label>Answer</label>
                            </div>
                      </div>
                    </div>
                </div>
                    @foreach($single->que_ans as $val)
                             @php
                             $uCode=uniqueCode();
                             @endphp

                    <div class="row" id="">
                      <div class="main-div-class">
                        <div class="col-md-4">
                        <input type="hidden" name="mcqs[{{$uCode}}][id]" value="{{$val->id}}">
                                <div class="form-group">
                                <input type="text" name="mcqs[{{$uCode}}][question]" value="{{$val->v_question}}" placeholder="Enter Question" class="form-control" required/>
                                </div>
                        </div>
                        <div class="col-md-4 add-opts">
                            <div class="row">
                                @foreach(json_decode($val->v_options) as $k=>$v)
                                @if($k=='0')
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <input type="text" name="mcqs[{{$uCode}}][option][{{$k}}]" value="{{$v}}" placeholder="Enter Options" class="form-control" required/>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" name="add" id="add-btn2" class="btn btn-success form-control">Add</button>
                                    </div>
                                @else
                                    <div class="row remove-single-class">
                                        <div class="col-md-6">
                                             <div class="form-group">
                                               <input type="text" name="mcqs[{{$uCode}}][option][{{$k}}]" value="{{$v}}" placeholder="Enter Options" class="form-control" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                       <!-- <a href="{{route('admin.lesson.delete_option',['id'=>$val->id,'opts'=>$v])}}"><button type="button" name="" id="" class="btn btn-danger form-control">Remove</button> </a> -->
                                               <a href="javascript:void(0);" title="Delete" onclick="DeleteOpts(this,'{{route('admin.lesson.delete_option',[$val->id,$v])}}',2)"><button type="button" name="" id="" class="btn btn-danger form-control">Remove</button></a>
                                        </div>
                                    </div>
                                @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="col-md-2">
                                <div class="form-group">
                                     <input type="text" name="mcqs[{{$uCode}}][answer]" value="{{$val->v_answer}}" placeholder="Enter Answer" class="form-control" required/>
                                </div>
                        </div>

                        <div class="col-md-2">
                        <a href="javascript:void(0);" title="Delete" onclick="DeleteQueAns(this,'{{route('admin.lesson.delete-que-ans',$val->id)}}',2)"><button type="button" name="" id="" class="btn btn-danger form-control">Remove</button></a>
                        </div>

                      </div>
                    </div>
                    @endforeach
                    <div class="row" id="dynamicAddRemove1"></div>
                    <div class="col-md-2">
                          <div class="form-group">
                                <button type="button" name="add" id="add-btn1" class="btn btn-success form-control">Add More</button>
                          </div>
                    </div>

                @else
                <div class="container1">
                    <input type="hidden" name="mcqs[0][id]" value="0">
                    <h2 align="center" class="mb-4">Exam Type : Multiple Choice Questions</h2>
                    <div class="row" id="dynamicAddRemove1">
                      <div class="main-div-class">
                        <div class="col-md-4">
                                <div class="form-group">
                                <label>Question</label>
                                <input type="text" name="mcqs[0][question]" value="" placeholder="Enter Question" class="form-control">
                                </div>
                        </div>
                        <div class="col-md-4 add-opts">
                            <div class="row">
                                 <div class="col-md-6">
                                      <div class="form-group">
                                        <label>Options</label>
                                        <input type="text" name="mcqs[0][option][0]" value="" placeholder="Enter Options" class="form-control" />
                                       </div>
                                 </div>
                                 <div class="col-md-6">
                                        <label>Add Options</label>
                                         <button type="button" name="add" id="add-btn2" class="btn btn-success form-control">Add</button>
                                 </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                                <div class="form-group">
                                     <label>Answer</label>
                                     <input type="text" name="mcqs[0][answer]" value="" placeholder="Enter Answer" class="form-control" />
                                </div>
                        </div>

                        <div class="col-md-2">
                                <div class="form-group">
                                    <label>Add More</label>
                                    <button type="button" name="add" id="add-btn1" class="btn btn-success form-control">Add More</button>
                                </div>
                        </div>

                      </div>
                    </div>
                </div>
                @endif


                <div class="card-footer">
                    <button type="submit" class="btn btn-info">SUBMIT</button>
                    <a href="{{$urlList}}"><button type="button" class="btn btn-default ml-2">Cancel</button></a>
                </div>
            </div>
        </div>
    </div>
   </div>

</form>
@endsection
@push('jsfun')



<script type="text/javascript">
    $(document).ready(function() {

        var i =0;
        var j =0;
        var k =0;

        $("#add-btn1").click(function() {
            k=0;
            ++j;
            $("#dynamicAddRemove1").append('<div class="main-div-class"><div class="col-md-4"><div class="form-group"><input type="text" name="mcqs[' + j + '][question]" value="" placeholder="Enter Question" class="form-control" required></div></div> <div class="col-md-4 add-opts"><div class="row"><div class="col-md-6"><div class="form-group"><input type="text" name="mcqs[' + j + '][option][' + k + ']" value="" placeholder="Enter Options" class="form-control" required/></div></div><div class="col-md-6"><button type="button" name="add" id="add-btn2" class="btn btn-success form-control">Add</button></div></div></div><div class="col-md-2"><div class="form-group"><input type="text" name="mcqs[' + j + '][answer]" value="" placeholder="Enter Answer" class="form-control" required/></div></div><div class="col-md-2"><div class="form-group"><button type="button" name="remove" id="remove" class="btn btn-danger form-control remove">Remove</button></div></div></div>');
        });

        $("#add-btn").click(function() {
            ++i;
            $("#dynamicAddRemove").append('<tr><td><input type="text" name="moreFields[' + i + '][question]" placeholder="Enter Question" class="form-control" /></td><td><input type="text" name="moreFields[' + i + '][answer]" placeholder="Enter Answer" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
        });

        $(document).on('click', '#add-btn2', function() {
        var name=$(this).closest('.row').find('input').attr('name');
        name = name.slice(0, -3);
        $(this).closest('.main-div-class').find('.add-opts').append('<div class="option-class"><div class="form-group"><input type="text" name="'+name+'[]" value="" placeholder="Enter Options" class="form-control" required/></div><div class="form-group remove-opts"><button type="button" name="remove" id="remove-opts" class="btn btn-danger form-control remove">Remove</button></div></div>');
        });

        $(document).on('click', '#remove', function() {
            $(this).parents('.main-div-class').remove();
        });

        $(document).on('click', '#remove-opts', function() {
            $(this).parents('.option-class').remove();
        });

        $(document).on('click', '.remove-tr', function() {
            $(this).parents('tr').remove();
        });

        $('.container').hide();
        $('.container1').hide();

        $('#i_question_type').change(function() {
            var type = $('#i_question_type').val();
            // alert(type);
            if (type == 1) {
                $('.container').show();
                $('.container1').hide();
            }else if(type==2){
                $('.container').hide();
                $('.container1').show();
            }else {
                $('.container').hide();
            }
        });

    });
</script>
@endpush