@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="{{ cardClasses() }}">
                <div class="card-header">
                    <h3 class="card-title">{{ $single->getId() ? 'Edit Record' : 'Add Record' }}</h3>
                    {!! cardTools() !!}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name {!! _star()!!}</label>
                                <input type="text" class="form-control" name="v_fname" value="{!! $single->name !!}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email{!! _star()!!}</label>
                                <input type="email" class="form-control" name="v_email" value="{!! $single->email !!}" required>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password{!! _star()!!}</label>
                                <input type="password" class="form-control" name="v_password" value="{!! $single->password !!}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password{!! _star()!!}</label>
                                <input type="password" class="form-control" name="c_password" value="{!! $single->password !!}" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Profile Image</label>
                            <input type="file" name="v_image" class="form-control" {{ $single->getId() ? '' : 'required'}} />
                            @if($single->getId())
                            <div class="mt-2"><img src="{{URL::asset('public/uploads/admin/'.$single->v_image)}}" alt=""></div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Status {!! _star()!!}</label>
                                <select class="form-control select2" name="ti_status" required data-parsley-errors-container=".err-estatus">
                                    <option value=""></option>
                                    {!! makeDropdown(siteConfig('ti_status'), $single->getStatus()) !!}
                                </select>
                                <div class="err-estatus"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">SUBMIT</button>
                    <a href="{{$urlList}}"><button type="button" class="btn btn-default ml-2">Cancel</button></a>
                </div>
            </div>
        </div>
    </div>
    </div>

</form>
@endsection
@push('jsfun')
<script type="text/javascript">

</script>
@endpush