<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-dark-info">
  <!-- Brand Logo -->
  <a href="javascript:;" class="brand-link text-center">
    <span class="brand-text font-weight-light" style="font-size:20px !important;">{{siteName()}} Admin</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{adminAssets('dist/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="javascript:;" class="d-block">{{_admin('name')}}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-compact nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->
        <?php
        $menuArr = [
          ['route' => 'admin.dashboard', 'title' => 'Dashboard', 'icon' => 'nav-icon fas fa-tachometer-alt'],
          ['route' => 'admin.adminmanagement.index', 'title' => 'Admin Management', 'icon' => 'nav-icon fas fa-user', 'extra_routes' => ['admin.adminmanagement.add', 'admin.adminmanagement.edit']],
          [
            'route' => '',
            'title' => 'General Settings',
            'icon' => 'nav-icon fas fa-cogs',
            'sub_links' => [
              ['route' => 'admin.sitesetting.index', 'title' => 'Site Settings'],
              ['route' => 'admin.settings.analytics', 'title' => 'Analytics Settings'],
              ['route' => 'admin.seofield.index', 'title' => 'SEO Field', 'extra_routes' => ['admin.seofield.add', 'admin.seofield.edit']],
            ]
          ],
          [
            'route' => '',
            'title' => 'Email Template Settings',
            'icon' => 'nav-icon fas fa-envelope',
            'sub_links' => [
              ['route' => 'admin.emailtemplateheader.index', 'title' => 'Email Template Header', 'extra_routes' => ['admin.emailtemplateheader.add', 'admin.emailtemplateheader.edit']],
              ['route' => 'admin.emailtemplatefooter.index', 'title' => 'Email Template Footer', 'extra_routes' => ['admin.emailtemplatefooter.add', 'admin.emailtemplatefooter.edit']],
              ['route' => 'admin.emailtemplate.index', 'title' => 'Email Template', 'extra_routes' => ['admin.emailtemplate.add', 'admin.emailtemplate.edit']],
            ]
          ],
          // [
          //   'route' => '',
          //   'title' => 'Media',
          //   'icon' => 'nav-icon fas fa-photo-video',
          //   'sub_links' => [
          //     ['route' => 'admin.media.index', 'title' => 'Media'],
          //     ['route' => 'admin.media.add', 'title' => 'Upload Media', 'target' => 'target="_blank"'],
          //   ]
          // ],
          ['route' => 'admin.subject.index', 'title' => 'Subjects', 'icon' => 'nav-icon fas fa-book-open', 'extra_routes' => ['admin.subject.add', 'admin.subject.edit']],
          ['route' => 'admin.topic.index', 'title' => 'Topics', 'icon' => 'nav-icon fas fa-book-reader', 'extra_routes' => ['admin.topic.add', 'admin.topic.edit']],
          ['route' => 'admin.lesson.index', 'title' => 'Lessons', 'icon' => 'nav-icon fas fa-graduation-cap', 'extra_routes' => ['admin.lesson.add', 'admin.lesson.edit']],
          // ['route' => 'admin.category.index', 'title' => 'Category', 'icon' => 'nav-icon fas fa-list-alt', 'extra_routes' => ['admin.category.add', 'admin.category.edit']],
          // ['route' => 'admin.product.index', 'title' => 'Products', 'icon' => 'nav-icon fas fa-gifts', 'extra_routes' => ['admin.product.add', 'admin.product.edit']],
          // ['route' => 'admin.customer.index', 'title' => 'Customers', 'icon' => 'nav-icon fas fa-users', 'extra_routes' => ['admin.customer.add', 'admin.customer.edit']],
          ['route' => 'admin.page.index', 'title' => 'Pages', 'icon' => 'nav-icon fas fa-book', 'extra_routes' => ['admin.page.add', 'admin.page.edit']],
          ['route' => 'admin.menu.index', 'title' => 'Header_Menus', 'icon' => 'nav-icon fas fa-bars', 'extra_routes' => ['admin.menu.add', 'admin.menu.edit']],
      
         
          [
            'route' => '',
            'title' => 'Leavel Menu Demo',
            'icon' => 'nav-icon far fa-circle',
            'sub_links' => [
              ['route' => '', 'title' => 'Leavel Menu 1', 'extra_routes' => ['']],
              ['route' => '', 'title' => 'Leavel Menu 2', 'extra_routes' => ['']],
            ]
          ],
          ['route' => 'admin.logout', 'title' => 'Logout', 'icon' => 'nav-icon fas fa-power-off'],
        ];

        foreach ($menuArr as $k => $menu) {
          $menu['active'] = '';
          if (routeActive($menu['route'])) {
            $menu['active'] = 'active';
          }
          if (isset($menu['extra_routes'])) {
            foreach ($menu['extra_routes'] as $r)
              if (routeActive($r))
                $menu['active'] = 'active';
          }

          if (isset($menu['sub_links']) && $menu['sub_links']) {
            foreach ($menu['sub_links'] as $ks => $submenu) {
              $submenu['active'] = '';
              if (routeActive($submenu['route'])) {
                $submenu['active'] = $menu['active'] = 'active';
              }
              if (isset($submenu['extra_routes'])) {
                foreach ($submenu['extra_routes'] as $r)
                  if (routeActive($r))
                    $submenu['active'] = $menu['active'] = 'active';
              }
              $menu['sub_links'][$ks] = $submenu;
            }
          }

          $menuArr[$k] = $menu;
        }

        foreach ($menuArr as $menu) {
          if (isset($menu['sub_links']) && $menu['sub_links']) {
        ?>
            <li class="nav-item has-treeview {{$menu['active'] ? 'menu-open' : ''}}">
              <a href="javascript:;" class="nav-link {{$menu['active']}}">
                <i class="{{$menu['icon'] ?? 'far fa-circle nav-icon'}}" style="vertical-align:text-top;"></i>
                <p>
                  {{$menu['title']}}
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php foreach ($menu['sub_links'] as $submenu) { ?>
                  <li class="nav-item">
                    <a href="{{ routePut($submenu['route']) }}" class="nav-link {{$submenu['active']}}" {!! $submenu['target'] ?? '' !!}>
                      <i class="far fa-circle nav-icon" style="vertical-align:text-top;"></i>
                      <p>{{$submenu['title']}}</p>
                    </a>
                  </li>
                <?php } ?>
              </ul>
            </li>
          <?php
          } else {
          ?>
            <li class="nav-item">
              <a href="{{ routePut($menu['route']) }}" class="nav-link {{$menu['active']}}">
                <i class="{{$menu['icon'] ?? 'far fa-circle nav-icon'}}" style="vertical-align:text-top;"></i>
                <p>{{$menu['title']}}</p>
              </a>
            </li>
        <?php
          }
        }
        ?>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>