@extends('admin.layouts.app')
@section('title', $title)

@section('content')

<form id="crud-frm" method="post" action="{{ $urlSave }}" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="{{ cardClasses() }}">
        <div class="card-header">
          <h3 class="card-title">{{ $single->getId() ? 'Edit Record' : 'Add Record' }}</h3>
          {!! cardTools() !!}
        </div>
        <div class="card-body">
        	<div class="row">
          	<div class="col-md-6">
              <div class="form-group">
                <label>First Name {!! _star()!!}</label>
                <input type="text" class="form-control" name="v_first_name" value="{!! $single->getFirstName() !!}" required >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Last Name {!! _star()!!}</label>
                <input type="text" class="form-control" name="v_last_name" value="{!! $single->getLastName() !!}" required >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Email {!! _star()!!}</label>
                <input type="email" class="form-control" id="v_email" name="v_email" value="{!! $single->getEmail() !!}" required onblur="checkUnique('v_email')" >
                <div id="uniqueInfo" ></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Phone {!! _star()!!}</label>
                <input type="text" class="form-control" name="v_phone" value="{!! $single->getPhone() !!}" required >
              </div>
            </div>
          </div>
          <div class="row">
          	<div class="col-md-6">
              <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control mb-2" id="v_password" name="v_password" value="" />
                <div class="icheck-primary d-inline">
                  <input type="checkbox" id="i_update_password" name="i_update_password" onchange="updatePassword()" />
                  <label for="i_update_password">Update Password [Check only if you want to update password]</label>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Status {!! _star()!!}</label>
                <select class="form-control select2" name="ti_status" required data-parsley-errors-container=".err-estatus" >
                	<option value="" ></option>
                	{!! makeDropdown(siteConfig('ti_status'), $single->getStatus()) !!}
                </select>
                <div class="err-estatus" ></div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info">SUBMIT</button>
          <a href="{{$urlList}}" ><button type="button" class="btn btn-default ml-2" >Cancel</button></a>
        </div>
      </div>
    </div>
  </div>
  
  
</form>
@endsection
@push('jsfun')
<script type="text/javascript" >
	var urlCheckUnique = '{{ $urlCheckUnique }}';
	function updatePassword(){
		jQuery('#v_password').prop('required', jQuery('#i_update_password').is(':checked'));
	}
</script>
@endpush
