<?php
return [
	
	// CRUD
	'added' => ':name added successfully.',
	'updated' => ':name updated successfully.',
	'deleted' => ':name deleted successfully.',
	
	'activated' => ':name activated successfully.',
	'deactivated' => ':name deactivated successfully.',
	
	'updated_settings' => 'Settings added successfully.',	
	
];
	