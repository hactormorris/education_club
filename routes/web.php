<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/////////////////////////////////////////// ADMIN ROUTES ///////////////////////////////////////////
Route::group(['prefix' => 'admin', 'namespace' => '\App\Http\Controllers\Backend', 'middleware' => []], function () {

	// Without Login
	Route::get('login', ['uses' => 'AuthController@login'])->name('admin.login');
	Route::post('submit-login', ['uses' => 'AuthController@loginSubmit'])->name('admin.submit.login');
	Route::get('logout', ['uses' => 'AuthController@logout'])->name('admin.logout');

	// With Login
	Route::group(['prefix' => '', 'middleware' => ['admin']], function () {

		// Dashboard
		Route::get('', ['uses' => 'DashboardController@index'])->name('admin.dashboard');

		// General Settings
		Route::group(['prefix' => 'settings'], function () {
			Route::post('submit', ['uses' => 'SettingsController@submit'])->name('admin.settings.submit');

			Route::get('site', ['uses' => 'SettingsController@site'])->name('admin.settings.site');
			Route::get('analytics', ['uses' => 'SettingsController@analytics'])->name('admin.settings.analytics');
		});

		// Media
		Route::group(['prefix' => 'media'], function () {
			Route::any('', ['uses' => 'MediaController@index'])->name('admin.media.index');
			Route::get('add', ['uses' => 'MediaController@add'])->name('admin.media.add');
			Route::post('save/{id}', ['uses' => 'MediaController@save'])->name('admin.media.save');
			Route::post('delete', ['uses' => 'MediaController@delete'])->name('admin.media.delete');
		});

		// Category
		Route::group(['prefix' => 'category'], function () {
			Route::any('', ['uses' => 'CategoryController@index'])->name('admin.category.index');
			Route::get('add', ['uses' => 'CategoryController@add'])->name('admin.category.add');
			Route::get('edit/{id}', ['uses' => 'CategoryController@edit'])->name('admin.category.edit');
			Route::post('save/{id}', ['uses' => 'CategoryController@save'])->name('admin.category.save');
			Route::post('delete', ['uses' => 'CategoryController@delete'])->name('admin.category.delete');
			Route::post('status-change', ['uses' => 'CategoryController@statusChange'])->name('admin.category.status-change');
			Route::post('update-order', ['uses' => 'CategoryController@updateOrder'])->name('admin.category.update-order');
			Route::any('check-unique', ['uses' => 'CategoryController@checkUnique'])->name('admin.category.check-unique');
		});

		// Product
		Route::group(['prefix' => 'product'], function () {
			Route::any('', ['uses' => 'ProductController@index'])->name('admin.product.index');
			Route::get('add', ['uses' => 'ProductController@add'])->name('admin.product.add');
			Route::get('edit/{id}', ['uses' => 'ProductController@edit'])->name('admin.product.edit');
			Route::post('save/{id}', ['uses' => 'ProductController@save'])->name('admin.product.save');
			Route::post('delete', ['uses' => 'ProductController@delete'])->name('admin.product.delete');
			Route::post('status-change', ['uses' => 'ProductController@statusChange'])->name('admin.product.status-change');
			Route::post('update-order', ['uses' => 'ProductController@updateOrder'])->name('admin.product.update-order');
			Route::any('check-unique', ['uses' => 'ProductController@checkUnique'])->name('admin.product.check-unique');
			Route::any('delete-image', ['uses' => 'ProductController@deleteImage'])->name('admin.product.delete-image');
		});

		// Customers
		Route::group(['prefix' => 'customer'], function () {
			Route::any('', ['uses' => 'CustomerController@index'])->name('admin.customer.index');
			Route::get('add', ['uses' => 'CustomerController@add'])->name('admin.customer.add');
			Route::get('edit/{id}', ['uses' => 'CustomerController@edit'])->name('admin.customer.edit');
			Route::post('save/{id}', ['uses' => 'CustomerController@save'])->name('admin.customer.save');
			Route::post('delete', ['uses' => 'CustomerController@delete'])->name('admin.customer.delete');
			Route::post('status-change', ['uses' => 'CustomerController@statusChange'])->name('admin.customer.status-change');
			Route::any('check-unique', ['uses' => 'CustomerController@checkUnique'])->name('admin.customer.check-unique');
		});

		// Pages
		Route::group(['prefix' => 'page'], function () {
			Route::any('', ['uses' => 'PageController@index'])->name('admin.page.index');
			Route::get('add', ['uses' => 'PageController@add'])->name('admin.page.add');
			Route::get('edit/{id}', ['uses' => 'PageController@edit'])->name('admin.page.edit');
			Route::post('save/{id}', ['uses' => 'PageController@save'])->name('admin.page.save');
			Route::post('delete', ['uses' => 'PageController@delete'])->name('admin.page.delete');
			Route::post('status-change', ['uses' => 'PageController@statusChange'])->name('admin.page.status-change');
			Route::any('check-unique', ['uses' => 'PageController@checkUnique'])->name('admin.page.check-unique');
			Route::any('delete-one/{id}', ['uses' => 'PageController@deleteOne'])->name('admin.page.delete-one');
			Route::any('status-update/{id}', ['uses' => 'PageController@statusupdate'])->name('admin.page.statusupdate');
		});


		// Seo-Field
		Route::group(['prefix' => 'seo-field'], function () {
			Route::any('', ['uses' => 'SeoFieldController@index'])->name('admin.seofield.index');
			Route::get('add', ['uses' => 'SeoFieldController@add'])->name('admin.seofield.add');
			Route::get('edit/{id}', ['uses' => 'SeoFieldController@edit'])->name('admin.seofield.edit');
			Route::post('save/{id}', ['uses' => 'SeoFieldController@save'])->name('admin.seofield.save');
			Route::post('delete', ['uses' => 'SeoFieldController@delete'])->name('admin.seofield.delete');
			Route::post('status-change', ['uses' => 'SeoFieldController@statusChange'])->name('admin.seofield.status-change');
			Route::any('delete-one/{id}', ['uses' => 'SeoFieldController@deleteOne'])->name('admin.seofield.delete-one');
			Route::any('status-update/{id}', ['uses' => 'SeoFieldController@statusupdate'])->name('admin.seofield.statusupdate');
		});


		// EmailHeaderTemplate
		Route::group(['prefix' => 'email-header-template'], function () {
			Route::any('', ['uses' => 'EmailTemplateHeaderController@index'])->name('admin.emailtemplateheader.index');
			Route::get('add', ['uses' => 'EmailTemplateHeaderController@add'])->name('admin.emailtemplateheader.add');
			Route::get('edit/{id}', ['uses' => 'EmailTemplateHeaderController@edit'])->name('admin.emailtemplateheader.edit');
			Route::post('save/{id}', ['uses' => 'EmailTemplateHeaderController@save'])->name('admin.emailtemplateheader.save');
			Route::post('delete', ['uses' => 'EmailTemplateHeaderController@delete'])->name('admin.emailtemplateheader.delete');
			Route::post('status-change', ['uses' => 'EmailTemplateHeaderController@statusChange'])->name('admin.emailtemplateheader.status-change');
			Route::any('delete-one/{id}', ['uses' => 'EmailTemplateHeaderController@deleteOne'])->name('admin.emailtemplateheader.delete-one');
			Route::any('status-update/{id}', ['uses' => 'EmailTemplateHeaderController@statusupdate'])->name('admin.emailtemplateheader.statusupdate');
		});


		// EmailFooterTemplate
		Route::group(['prefix' => 'email-footer-template'], function () {
			Route::any('', ['uses' => 'EmailTemplateFooterController@index'])->name('admin.emailtemplatefooter.index');
			Route::get('add', ['uses' => 'EmailTemplateFooterController@add'])->name('admin.emailtemplatefooter.add');
			Route::get('edit/{id}', ['uses' => 'EmailTemplateFooterController@edit'])->name('admin.emailtemplatefooter.edit');
			Route::post('save/{id}', ['uses' => 'EmailTemplateFooterController@save'])->name('admin.emailtemplatefooter.save');
			Route::post('delete', ['uses' => 'EmailTemplateFooterController@delete'])->name('admin.emailtemplatefooter.delete');
			Route::post('status-change', ['uses' => 'EmailTemplateFooterController@statusChange'])->name('admin.emailtemplatefooter.status-change');
			Route::any('delete-one/{id}', ['uses' => 'EmailTemplateFooterController@deleteOne'])->name('admin.emailtemplatefooter.delete-one');
			Route::any('status-update/{id}', ['uses' => 'EmailTemplateFooterController@statusupdate'])->name('admin.emailtemplatefooter.statusupdate');
		});


		// EmailTemplate
		Route::group(['prefix' => 'email-template'], function () {
			Route::any('', ['uses' => 'EmailTemplateController@index'])->name('admin.emailtemplate.index');
			Route::get('add', ['uses' => 'EmailTemplateController@add'])->name('admin.emailtemplate.add');
			Route::get('edit/{id}', ['uses' => 'EmailTemplateController@edit'])->name('admin.emailtemplate.edit');
			Route::post('save/{id}', ['uses' => 'EmailTemplateController@save'])->name('admin.emailtemplate.save');
			Route::post('delete', ['uses' => 'EmailTemplateController@delete'])->name('admin.emailtemplate.delete');
			Route::post('status-change', ['uses' => 'EmailTemplateController@statusChange'])->name('admin.emailtemplate.status-change');
			Route::any('single-delete/{id}', ['uses' => 'EmailTemplateController@singleDelete'])->name('admin.emailtemplate.single-delete');
			Route::any('status-update/{id}', ['uses' => 'EmailTemplateController@statusUpdate'])->name('admin.emailtemplate.statusupdate');
			Route::any('concate', ['uses' => 'EmailTemplateController@concate'])->name('admin.emailtemplate.concate');
		});


		// Admin Management
		Route::group(['prefix' => 'admin-management'], function () {
			Route::any('', ['uses' => 'AdminController@index'])->name('admin.adminmanagement.index');
			Route::get('add', ['uses' => 'AdminController@add'])->name('admin.adminmanagement.add');
			Route::get('edit/{id}', ['uses' => 'AdminController@edit'])->name('admin.adminmanagement.edit');
			Route::post('save/{id}', ['uses' => 'AdminController@save'])->name('admin.adminmanagement.save');
			Route::post('delete', ['uses' => 'AdminController@delete'])->name('admin.adminmanagement.delete');
			Route::post('status-change', ['uses' => 'AdminController@statusChange'])->name('admin.adminmanagement.status-change');
			Route::any('delete-one/{id}', ['uses' => 'AdminController@deleteOne'])->name('admin.adminmanagement.delete-one');
			Route::any('status-update/{id}', ['uses' => 'AdminController@statusupdate'])->name('admin.adminmanagement.statusupdate');
		});


		// Subject
		Route::group(['prefix' => 'subject'], function () {
			Route::any('', ['uses' => 'SubjectController@index'])->name('admin.subject.index');
			Route::get('add', ['uses' => 'SubjectController@add'])->name('admin.subject.add');
			Route::get('edit/{id}', ['uses' => 'SubjectController@edit'])->name('admin.subject.edit');
			Route::post('save/{id}', ['uses' => 'SubjectController@save'])->name('admin.subject.save');
			Route::post('delete', ['uses' => 'SubjectController@delete'])->name('admin.subject.delete');
			Route::post('status-change', ['uses' => 'SubjectController@statusChange'])->name('admin.subject.status-change');
			Route::any('delete-one/{id}', ['uses' => 'SubjectController@deleteOne'])->name('admin.subject.delete-one');
			Route::any('status-update/{id}', ['uses' => 'SubjectController@statusupdate'])->name('admin.subject.statusupdate');
		});


		// Topic
		Route::group(['prefix' => 'topic'], function () {
			Route::any('', ['uses' => 'TopicController@index'])->name('admin.topic.index');
			Route::get('add', ['uses' => 'TopicController@add'])->name('admin.topic.add');
			Route::get('edit/{id}', ['uses' => 'TopicController@edit'])->name('admin.topic.edit');
			Route::post('save/{id}', ['uses' => 'TopicController@save'])->name('admin.topic.save');
			Route::post('delete', ['uses' => 'TopicController@delete'])->name('admin.topic.delete');
			Route::post('status-change', ['uses' => 'TopicController@statusChange'])->name('admin.topic.status-change');
			Route::any('delete-one/{id}', ['uses' => 'TopicController@deleteOne'])->name('admin.topic.delete-one');
			Route::any('status-update/{id}', ['uses' => 'TopicController@statusupdate'])->name('admin.topic.statusupdate');
		});


		// Lesson
		Route::group(['prefix' => 'lesson'], function () {
			Route::any('', ['uses' => 'LessonController@index'])->name('admin.lesson.index');
			Route::get('add', ['uses' => 'LessonController@add'])->name('admin.lesson.add');
			Route::get('edit/{id}', ['uses' => 'LessonController@edit'])->name('admin.lesson.edit');
			Route::post('save/{id}', ['uses' => 'LessonController@save'])->name('admin.lesson.save');
			Route::post('delete', ['uses' => 'LessonController@delete'])->name('admin.lesson.delete');
			Route::post('status-change', ['uses' => 'LessonController@statusChange'])->name('admin.lesson.status-change');
			Route::any('delete-one/{id}', ['uses' => 'LessonController@deleteOne'])->name('admin.lesson.delete-one');
			Route::any('status-update/{id}', ['uses' => 'LessonController@statusupdate'])->name('admin.lesson.statusupdate');
			Route::any('delete-que-ans/{id}', ['uses' => 'LessonController@delete_que_ans'])->name('admin.lesson.delete-que-ans');
			Route::any('delete_option/{id}/{opts}', ['uses' => 'LessonController@delete_option'])->name('admin.lesson.delete_option');

		});


		// Header-Menu
		Route::group(['prefix' => 'header-menu'], function () {
			Route::any('', ['uses' => 'MenuController@index'])->name('admin.menu.index');
			Route::get('add', ['uses' => 'MenuController@add'])->name('admin.menu.add');
			Route::get('edit/{id}', ['uses' => 'MenuController@edit'])->name('admin.menu.edit');
			Route::post('save/{id}', ['uses' => 'MenuController@save'])->name('admin.menu.save');
			Route::post('delete', ['uses' => 'MenuController@delete'])->name('admin.menu.delete');
			Route::post('status-change', ['uses' => 'MenuController@statusChange'])->name('admin.menu.status-change');
			Route::any('delete-one/{id}', ['uses' => 'MenuController@deleteOne'])->name('admin.menu.delete-one');
			Route::any('status-update/{id}', ['uses' => 'MenuController@statusupdate'])->name('admin.menu.statusupdate');
		});


		//SiteSettings
		Route::group(['prefix' => 'site-setting'], function () {
			Route::get('/', ['uses' => 'SiteSettingController@index'])->name('admin.sitesetting.index');
			Route::post('store', ['uses' => 'SiteSettingController@store'])->name('admin.sitesetting.store');
		});
	});
});
/////////////////////////////////////////// ADMIN ROUTES ///////////////////////////////////////////


/////////////////////////////////////////// FRONT ROUTES ///////////////////////////////////////////
Route::group(['prefix' => '', 'namespace' => '\App\Http\Controllers\Front', 'middleware' => []], function () {
	Route::get('', ['uses' => 'CommonController@index'])->name('home');
});
/////////////////////////////////////////// FRONT ROUTES ///////////////////////////////////////////